﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EpamLabs.TicketResale.Data
{
    public abstract class InMemoryGenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        protected ICollection<TEntity> _collection;

        public InMemoryGenericRepository(IInMemoryDataStore dataStore)
        {
            _collection = dataStore.GetCollection<TEntity>();
        }

        public abstract TEntity GetById(object id);

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _collection;
        }

        public IEnumerable<TEntity> Find(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IEnumerable<TEntity> q = _collection;
            
            if (filter != null)
            {
                q = _collection.Where(filter.Compile());
            }

            if (orderBy != null)
            {
                return orderBy(q.AsQueryable())?.ToList();
            }
            else
            {
                return q;
            }
        }

        public virtual TEntity Add(TEntity entity)
        {
            _collection.Add(entity);
            return entity;
        }

        public abstract TEntity Update(TEntity entity);

        public virtual TEntity Delete(TEntity entity)
        {
            if (_collection.Remove(entity))
            {
                return entity;
            }
            return null;
        }

        public IQueryable<TEntity> GetQueryable()
        {
            return _collection.AsQueryable();
        }
    }
}
