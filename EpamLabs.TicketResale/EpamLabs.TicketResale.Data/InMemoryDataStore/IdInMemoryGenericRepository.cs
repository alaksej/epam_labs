﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public class IdInMemoryGenericRepository<TKey, TEntity> : InMemoryGenericRepository<TEntity>
        where TEntity : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        public IdInMemoryGenericRepository(IInMemoryDataStore dataStore) : base(dataStore)
        {
        }

        public override TEntity GetById(object id)
        {
            var key = (TKey)id;
            return _collection.FirstOrDefault(x => x.Id.Equals(key));
        }

        public override TEntity Update(TEntity entity)
        {
            var old = GetById(entity.Id);
            if (_collection.Remove(old))
            {
                _collection.Add(entity);
                return entity;
            }
            return null;
        }
    }
}
