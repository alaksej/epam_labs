﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Linq;

namespace EpamLabs.TicketResale.Data
{
    public class InMemoryUserTokenRepository : InMemoryGenericRepository<IdentityUserToken<string>>
    {
        public InMemoryUserTokenRepository(IInMemoryDataStore dataStore) : base(dataStore)
        {
        }

        public override IdentityUserToken<string> GetById(object id)
        {
            var key = id as IdentityUserToken<string>;
            return GetById(key);
        }

        public override IdentityUserToken<string> Update(IdentityUserToken<string> entity)
        {
            var old = GetById(entity);
            if (_collection.Remove(old))
            {
                _collection.Add(entity);
                return entity;
            }
            return null;
        }

        private IdentityUserToken<string> GetById(IdentityUserToken<string> key)
        {
            return key == null ? null : _collection.FirstOrDefault(x => x.UserId == key.UserId && x.LoginProvider == key.LoginProvider && x.Name == key.Name);
        }
    }
}
