﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public class EmptyInMemoryDataStore : IInMemoryDataStore
    {
        public ICollection<ApplicationUser> Users { get; set; }
        public ICollection<ApplicationRole> Roles { get; set; }
        public ICollection<IdentityUserRole<string>> UserRoles { get; set; }
        public ICollection<City> Cities { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<Venue> Venues { get; set; }

        public EmptyInMemoryDataStore()
        {
            Users = new List<ApplicationUser>();
            Roles = new List<ApplicationRole>();
            UserRoles = new List<IdentityUserRole<string>>();
            Cities = new List<City>();
            Events = new List<Event>();
            Orders = new List<Order>();
            Tickets = new List<Ticket>();
            Venues = new List<Venue>();
        }

        public ICollection<TEntity> GetCollection<TEntity>()
        {
            var needed = typeof(ICollection<TEntity>);
            var prop = GetType().GetProperties()
                .Where(x => x.PropertyType == needed)
                .FirstOrDefault();
            return (ICollection<TEntity>)prop?.GetValue(this);
        }
    }
}
