﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Linq;

namespace EpamLabs.TicketResale.Data
{
    public class InMemoryUserRoleRepository : InMemoryGenericRepository<IdentityUserRole<string>>
    {
        public InMemoryUserRoleRepository(IInMemoryDataStore dataStore) : base(dataStore)
        {
        }

        public override IdentityUserRole<string> GetById(object id)
        {
            var key = id as IdentityUserRole<string>;
            return GetById(key);
        }

        public override IdentityUserRole<string> Update(IdentityUserRole<string> entity)
        {
            var old = GetById(entity);
            if (_collection.Remove(old))
            {
                _collection.Add(entity);
                return entity;
            }
            return null;
        }

        private IdentityUserRole<string> GetById(IdentityUserRole<string> key)
        {
            return key == null ? null : _collection.FirstOrDefault(x => x.RoleId == key.RoleId && x.UserId == key.UserId);
        }
    }
}
