﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{ 
    public interface IInMemoryDataStore
    {
        ICollection<ApplicationUser> Users { get; set; }
        ICollection<ApplicationRole> Roles { get; set; }
        ICollection<IdentityUserRole<string>> UserRoles { get; set; }
        ICollection<City> Cities { get; set; }
        ICollection<Event> Events { get; set; }
        ICollection<Order> Orders { get; set; }
        ICollection<Ticket> Tickets { get; set; }
        ICollection<Venue> Venues { get; set; }
        ICollection<TEntity> GetCollection<TEntity>();
    }
}
