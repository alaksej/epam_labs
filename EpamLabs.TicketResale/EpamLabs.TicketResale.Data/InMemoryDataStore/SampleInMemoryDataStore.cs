﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public class SampleInMemoryDataStore : EmptyInMemoryDataStore
    {
        public SampleInMemoryDataStore(IPasswordHasher<ApplicationUser> hasher)
        {
            // Users
            //var hasher = new PasswordHasher<ApplicationUser>();
            var admin = new ApplicationUser
            {
                UserName = "Admin",
                NormalizedUserName = "ADMIN",
                FirstName = "Alex",
                LastName = "Yankou",
                Email = "yankouav@gmail.com",
                Locale = "be",
                PhoneNumber = "+357291054402",
                PhoneNumberConfirmed = true,
                Address = "5ty Tranzitny zav, 3",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            var hash = hasher.HashPassword(admin, "Admin");
            admin.PasswordHash = hash;

            var user1 =  new ApplicationUser
            {
                UserName = "User",
                NormalizedUserName = "USER",
                FirstName = "John",
                LastName = "Smith",
                Email = "john.smith@example.com",
                Locale = "en",
                PhoneNumber = "+19360000000",
                Address = "5th Pkwy, LA, CA",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            user1.PasswordHash = hasher.HashPassword(user1, "User");
            var user2 = new ApplicationUser
            {
                UserName = "test",
                NormalizedUserName = "TEST",
                FirstName = "Ivan",
                LastName = "Petrov",
                Email = "ivan.petrov@example.com",
                Locale = "ru",
                PhoneNumber = "+75550000000",
                Address = "Berezovaya, 3, k.4, Moskva",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            user2.PasswordHash = hasher.HashPassword(user2, "test");

            Users = new List<ApplicationUser> { admin, user1, user2 };

            // Roles
            var roleAdmin = new ApplicationRole(EpamLabs.TicketResale.Data.Roles.Admin)
            {
                NormalizedName = EpamLabs.TicketResale.Data.Roles.Admin.ToUpper()
            };
            Roles = new List<ApplicationRole>
            {
                roleAdmin
            };

            // UserRoles
            var userRole1 = new IdentityUserRole<string>()
            {
                RoleId = roleAdmin.Id,
                UserId = admin.Id
            };

            UserRoles = new List<IdentityUserRole<string>>
            {
                userRole1
            };

            // Cities
            var gomel = new City { Id = 1, Name = "Gomel" };
            var minsk = new City { Id = 2, Name = "Minsk" };
            var LA = new City { Id = 3, Name = "Los Angeles" };
            var NY = new City { Id = 4, Name = "New York" };
            var moscow = new City { Id = 5, Name = "Moscow" };
            var peter = new City { Id = 6, Name = "St. Petersburg" };
            Cities = new List<City>
            {
                gomel,
                minsk,
                LA,
                NY,
                moscow,
                peter
            };

            // Venues
            var kvartirnik = new Venue
            {
                Id = 1,
                Name = "Bar Kvartirnik",
                Address = "Bilecki spusk, 1",
                CityId = gomel.Id,
            };
            var gck = new Venue
            {
                Id = 2,
                Name = "GCK",
                Address = "Irininskaya, 17",
                CityId = gomel.Id
            };
            var kupalauski = new Venue
            {
                Id = 3,
                Name = "Kupalauski theatre",
                Address = "220030 Рэспубліка Беларусь, Мінск, вул. Энгельса, 7",
                CityId = minsk.Id
            };
            var kzMinsk = new Venue
            {
                Id = 4,
                Name = "КЗ Минск",
                Address = "г. Минск, ул. Октябрьская, 5",
                CityId = minsk.Id
            };
            var universalStudios = new Venue
            {
                Id = 5,
                Name = "Universal Studios Hollywood",
                Address = "100 Universal City Plaza, Universal City, CA 91608, USA",
                CityId = LA.Id
            };
            var getty = new Venue
            {
                Id = 6,
                Name = "Getty Center",
                Address = "1200 Getty Center Dr, Los Angeles, CA 90049, USA",
                CityId = LA.Id
            };
            var metropolitan = new Venue
            {
                Id = 7,
                Name = "Metropolitan Museum of Art",
                Address = "1000 5th Ave, New York, NY 10028, USA",
                CityId = NY.Id
            };
            var madison = new Venue
            {
                Id = 8,
                Name = "Madison Square Garden",
                Address = "7th Ave & 32nd St, 10001 Manhattan, NY, US",
                CityId = NY.Id
            };
            var teatrEstrady = new Venue
            {
                Id = 9,
                Name = "Театр эстрады",
                Address = "Берсеневская наб., 20/2 м.Боровицкая ",
                CityId = moscow.Id
            };
            var movieMoscow = new Venue
            {
                Id = 10,
                Name = "5 звезд на Новокузнецкой",
                Address = "Б.Овчинниковский пер., 16, ТЦ «Аркадия» м.Новокузнецкая, Третьяковская ",
                CityId = moscow.Id
            };
            var kzAvrora = new Venue
            {
                Id = 11,
                Name = "Концертный зал Аврора",
                Address = "Пироговская наб., 5/2 м.Площадь Ленина ",
                CityId = peter.Id
            };
            var hermitage = new Venue
            {
                Id = 12,
                Name = "Эрмитаж",
                Address = "Дворцовая пл., 2",
                CityId = peter.Id
            };
            var minskArena = new Venue
            {
                Id = 13,
                Name = "Минск-Арена",
                Address = "пр-т Победителей, 111",
                CityId = minsk.Id
            };
            Venues = new List<Venue>
            {
                kvartirnik,
                gck,
                kupalauski,
                kzMinsk,
                universalStudios,
                getty,
                metropolitan,
                madison,
                teatrEstrady,
                movieMoscow,
                kzAvrora,
                hermitage,
                minskArena
            };

            // Events
            var tt34 = new Event
            {
                Id = 1,
                Image = "/images/tt-34-494472.jpg",
                Date = new DateTime(2016, 12, 10, 18, 0, 0),
                Description = "<p><strong>Ацетилен</strong></p>",
                Name = "TT'34",
                Url = "http://afisha.tut.by/concert-gomel/gomel_tt34/",
                VenueId = gck.Id
            };
            var clapton = new Event
            {
                Id = 2,
                Image = "/images/eric_clapton.jpg",
                Date = new DateTime(2017, 3, 19, 19, 30, 0),
                Description = @"<p><em>Legendary British guitarist, Eric Clapton (born March 30th, 1945) 
                                    has forged an extraordinary career lasting over six decades, playing 
                                    with some of the most influential groups of the 60s and 70s before 
                                    establishing his talents as a solo artist.</em></p>",
                Name = "Eric Clapton",
                Url = "https://www.songkick.com/concerts/28657314-eric-clapton-at-madison-square-garden",
                VenueId = madison.Id
            };
            var islamicArts = new Event
            {
                Id = 3,
                Image = "/images/islamic_art.jpg",
                Date = new DateTime(2017, 1, 21, 10, 30, 0),
                Description = @"<p>A tour of the new galleries for the Art of the Arab Lands, Turkey, 
                                    Iran, Central Asia, and Later South Asia explores the <a href='www.metmuseum.org'
                                    target='_blank'>
                                    Metropolitan Museum</a>'s collection of Islamic art, a collection that is one of the 
                                    finest and most comprehensive in the world. Fifteen galleries grouped 
                                    by geographic region trace the course of Islamic civilization from Spain 
                                    in the West to India in the East. The tour draws on this collection to 
                                    explore the rich artistic traditions of the Islamic world and the distinct 
                                    cultures within its fold.</p>",
                Name = "Arts of the Islamic World",
                Url = "http://www.metmuseum.org/events/programs/met-tours/guided-tours/arts-of-the-islamic-world",
                VenueId = metropolitan.Id
            };
            var javier = new Event
            {
                Id = 4,
                Image = "/images/javier.jpg",
                Date = new DateTime(2016, 12, 29, 22, 45, 0),
                Description = @"<p>Tour name: Delicious Tour</p>",
                Name = "Javier Rodríguez Macpherson",
                Url = "https://www.songkick.com/concerts/28685289-javier-rodriguez-macpherson-at-madison-square-garden",
                VenueId = madison.Id
            };

            var monaspectakl = new Event
            {
                Id = 5,
                Image = "/images/na-belarus_-bog-zhyve_1.jpg",
                Date = new DateTime(2016, 12, 12, 19, 0, 0),
                Description = @"<p>монаспектакль паводле твораў Уладзіміра Караткевіча</p>
                                <p>Усю разнастайную палітру абліччаў Караткевічавых герояў 
                                увасобіць на сцэне артыст Нацыянальнага акадэмічнага драматычнага 
                                тэатра імя Максіма Горкага Валерый Шушкевіч</p>",
                Name = "На Беларусі Бог жыве",
                Url = "http://kupalauski.by/performances/small_stage/on-god-long-live-belarus/",
                VenueId = kupalauski.Id
            };
            var brutto = new Event
            {
                Id = 6,
                Image = "/images/sergey-mikhalok-i-gruppa-brutto-7190625.jpg",
                Date = new DateTime(2017, 3, 8, 19, 0, 0),
                Description = @"<p><strong>Сергей Михалок и группа BRUTTO собирают «Минск-Арену» 8 марта, 19.00, «Минск-Арена»</strong><br/>
                                    Первый концерт группы BRUTTO в Гомеле собрал поклонников со всей страны: 5000 зрителей приехали, чтобы увидеть первое выступление музыкантов на родной земле. До последнего белорусы не верили, что Сергей Михалок и Ко появятся на сцене, однако состоявшийся концерт доказал обратное: BRUTTO устроили мощное трехчасовое шоу, продемонстрировав отличную форму и новое звучание своих бронебойных хитов.<br/>
                                    В программу «Double Hot» вошли лучшие треки BRUTTO из альбомов «Родны край» и «Underdog», а также золотые хиты «Ляписов» за последние 10 лет. Группа прокатилась с «Double Hot» по городам Украины, Америки и Европы, и, конечно, громко презентовала программу в Гомеле. Уже на родине к BRUTTO присоединилась знаменитая духовая секция «Ляписов» и, безусловно, звучание группы стало еще громче, сочнее и интереснее.<br/>
                                    В сентябре проекту BRUTTO исполнилось 2 года. С первых дней музыканты громко и уверенно заявили о себе и не обманули ожиданий фанатов по всему миру. За эти 2 года BRUTTO выпустили 2 студийных альбома - Underdog и «Родны край», сняли 17 клипов, представили 2 lyric-video, проехали 2 больших тура по Украине в поддержку альбомов «Underdog» и «Родны край», каждый из которых прошел с аншлагом в 30 городах. За это время BRUTTO были хедлайнерами всех крупнейших фестивалей Украины (Z-Games, Фестиваль ZАХІД, Файне Місто, Бандерштат, Схід-Рок, Respublica, PORTO FRANKO GOGOL FEST, Atlas Weekend и др.) и участниками фестивалей в Литве, Польше, Латвии, Молдове и Великобритании.<br/>
                                    <p>",
                Name = "Группа Brutto ",
                Url = "http://afisha.tut.by/concert/gruppa_brutto/?rr",
                VenueId = minskArena.Id
            };
            Events = new List<Event>
            {
                tt34,
                clapton,
                islamicArts,
                javier,
                monaspectakl,
                brutto
            };

            // Tickets
            var tt34Ticket1 = new Ticket
            {
                Id = 1,
                EventId = tt34.Id,
                Price = 15,
                UserId = admin.Id,
                State = TicketState.Sold
            };
            var tt34Ticket2 = new Ticket
            {
                Id = 2,
                EventId = tt34.Id,
                Price = 25,
                UserId = admin.Id,
                State = TicketState.Sold
            };
            var monaspectaklTicket1 = new Ticket
            {
                Id = 3,
                EventId = monaspectakl.Id,
                Price = 5,
                UserId = admin.Id,
                Comment = "Балкон",
                State = TicketState.ForSale
            };
            var claptonTicket1 = new Ticket
            {
                Id = 4,
                EventId = clapton.Id,
                Price = 95,
                UserId = user1.Id,
                State = TicketState.ForSale
            };
            var claptonTicket2 = new Ticket
            {
                Id = 5,
                EventId = clapton.Id,
                Price = 400,
                UserId = user1.Id,
                Comment = "Fan zone access",
                State = TicketState.Sold
            };
            var artsTicket1 = new Ticket
            {
                Id = 6,
                EventId = islamicArts.Id,
                Price = 7,
                UserId = user1.Id,
                State = TicketState.Ordered
            };
            var artsTicket2 = new Ticket
            {
                Id = 7,
                EventId = islamicArts.Id,
                Price = 7,
                UserId = user1.Id,
                State = TicketState.ForSale
            };
            var monaspectaklTicket2 = new Ticket
            {
                Id = 8,
                EventId = monaspectakl.Id,
                Price = 15,
                UserId = admin.Id,
                Comment = "Партэр",
                State = TicketState.ForSale
            };
            var javierTicket1 = new Ticket
            {
                Id = 9,
                EventId = javier.Id,
                Price = 70,
                UserId = user1.Id,
                State = TicketState.ForSale
            };
            var javierTicket2 = new Ticket
            {
                Id = 10,
                EventId = javier.Id,
                Price = 70,
                UserId = user1.Id,
                State = TicketState.ForSale
            };
            var claptonTicket3 = new Ticket
            {
                Id = 11,
                EventId = clapton.Id,
                Price = 155,
                UserId = user1.Id,
                State = TicketState.ForSale
            };
            var claptonTicket4 = new Ticket
            {
                Id = 12,
                EventId = clapton.Id,
                Price = 200,
                UserId = user1.Id,
                Comment = "Close to the stage",
                State = TicketState.ForSale
            };
            var bruttoTicket1 = new Ticket
            {
                Id = 13,
                EventId = brutto.Id,
                Price = 30,
                UserId = admin.Id,
                Comment = "фан-зона",
                State = TicketState.ForSale
            };
            var bruttoTicket2= new Ticket
            {
                Id = 14,
                EventId = brutto.Id,
                Price = 20,
                UserId = admin.Id,
                Comment = "танцпол",
                State = TicketState.Ordered
            };

            Tickets = new List<Ticket>
            {
                tt34Ticket1,
                tt34Ticket2,
                monaspectaklTicket1,
                monaspectaklTicket2,
                claptonTicket1,
                claptonTicket2,
                claptonTicket3,
                claptonTicket4,
                javierTicket1,
                javierTicket2,
                artsTicket1,
                artsTicket2,
                bruttoTicket1,
                bruttoTicket2
            };


            // Orders
            Orders = new List<Order> {
                new Order
                {
                    Id = 1,
                    UserId = user2.Id,
                    TicketId = tt34Ticket1.Id,
                    State = OrderState.Confirmed,
                    TrackingNumber = "124312314"
                },
                new Order
                {
                    Id = 2,
                    UserId = user2.Id,
                    TicketId = tt34Ticket2.Id,
                    State = OrderState.Confirmed,
                    TrackingNumber = "1234123423"
                },
                new Order
                {
                    Id = 3,
                    UserId = user2.Id,
                    TicketId = artsTicket1.Id,
                    State = OrderState.Pending,
                },
                new Order
                {
                    Id = 4,
                    UserId = admin.Id,
                    TicketId = claptonTicket2.Id,
                    State = OrderState.Confirmed,
                    TrackingNumber = "31423511243"
                },
                new Order
                {
                    Id = 5,
                    UserId = user2.Id,
                    TicketId = javierTicket1.Id,
                    State = OrderState.Rejected,
                },
                new Order
                {
                    Id = 6,
                    UserId = user2.Id,
                    TicketId = bruttoTicket2.Id,
                    State = OrderState.Pending,
                }
            };

        }
    }
}
