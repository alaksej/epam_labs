﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Linq;

namespace EpamLabs.TicketResale.Data
{
    public static class ApplicationDbContextExtensions
    {
        public static void EnsureSeedData(this ApplicationDbContext context)
        {
            if (context.AllMigrationsApplied())
            {
                if (!context.Users.Any())
                {
                    //roleManager.CreateAsync(new ApplicationRole(Roles.Admin));

                    // Users
                    var hasher = new PasswordHasher<ApplicationUser>();
                    var admin = new ApplicationUser
                    {
                        UserName = "Admin",
                        NormalizedUserName = "ADMIN",
                        FirstName = "Alex",
                        LastName = "Yankou",
                        Email = "yankouav@gmail.com",
                        Locale = "be",
                        PhoneNumber = "+375291054402",
                        PhoneNumberConfirmed = true,
                        Address = "5ty Tranzitny zav, 3",
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    admin.PasswordHash = hasher.HashPassword(admin, "Admin");
                    //userManager.CreateAsync(admin, "Admin").Wait();
                    //userManager.AddToRoleAsync(admin, Roles.Admin).Wait();
                    //admin = userManager.FindByNameAsync(admin.UserName).Result;
                    //context.Attach(admin);

                    var user1 = new ApplicationUser
                    {
                        UserName = "User",
                        NormalizedUserName = "USER",
                        FirstName = "John",
                        LastName = "Smith",
                        Email = "john.smith@example.com",
                        Locale = "en",
                        PhoneNumber = "+19360000000",
                        Address = "5th Pkwy, LA, CA",
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    user1.PasswordHash = hasher.HashPassword(user1, "User");
                    //userManager.CreateAsync(user1, "User").Wait();
                    //user1 = userManager.FindByNameAsync(user1.UserName).Result;
                    //context.Attach(user1);

                    var user2 = new ApplicationUser
                    {
                        UserName = "test",
                        NormalizedUserName = "TEST",
                        FirstName = "Ivan",
                        LastName = "Petrov",
                        Email = "ivan.petrov@example.com",
                        Locale = "ru",
                        PhoneNumber = "+75550000000",
                        Address = "Berezovaya, 3, k.4, Moskva",
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    user2.PasswordHash = hasher.HashPassword(user2, "test");
                    //userManager.CreateAsync(user2, "test").Wait();
                    //user2 = userManager.FindByNameAsync(user2.UserName).Result;
                    //context.Attach(user2);

                    context.Users.AddRange(admin, user1, user2);


                    // Roles
                    var roleAdmin = new ApplicationRole(Roles.Admin)
                    {
                        NormalizedName = Roles.Admin.ToUpper()
                    };
                    context.Roles.AddRange(roleAdmin);

                    // UserRoles
                    var userRole1 = new IdentityUserRole<string>()
                    {
                        RoleId = roleAdmin.Id,
                        UserId = admin.Id
                    };

                    context.UserRoles.AddRange(userRole1);

                    // Cities
                    var gomel = new City { Name = "Gomel" };
                    var minsk = new City { Name = "Minsk" };
                    var LA = new City { Name = "Los Angeles" };
                    var NY = new City { Name = "New York" };
                    var moscow = new City { Name = "Moscow" };
                    var peter = new City { Name = "St. Petersburg" };
                    var london = new City { Name = "London" };
                    var houston = new City { Name = "Houston" };
                    context.Cities.AddRange(
                        gomel,
                        minsk,
                        LA,
                        NY,
                        moscow,
                        peter,
                        london,
                        houston
                    );

                    // Venues
                    var hydePark = new Venue
                    {
                        Name = "Hyde park",
                        Address = "The Royal Parks, W2 2UH, London,UK",
                        City = london,
                    };
                    var hollywoodBowl = new Venue
                    {
                        Name = "Hollywood Bowl",
                        Address = "2301 Highland Avenue, Los Angeles, CA 90068",
                        City = LA,
                    };
                    var kvartirnik = new Venue
                    {
                        Name = "Bar Kvartirnik",
                        Address = "Bilecki spusk, 1",
                        City = gomel,
                    };
                    var gck = new Venue
                    {
                        Name = "GCK",
                        Address = "Irininskaya, 17",
                        City = gomel
                    };
                    var kupalauski = new Venue
                    {
                        Name = "Kupalauski theatre",
                        Address = "220030 Рэспубліка Беларусь, Мінск, вул. Энгельса, 7",
                        City = minsk
                    };
                    var kzMinsk = new Venue
                    {
                        Name = "КЗ Минск",
                        Address = "г. Минск, ул. Октябрьская, 5",
                        City = minsk
                    };
                    var universalStudios = new Venue
                    {
                        Name = "Universal Studios Hollywood",
                        Address = "100 Universal City Plaza, Universal City, CA 91608, USA",
                        City = LA
                    };
                    var getty = new Venue
                    {
                        Name = "Getty Center",
                        Address = "1200 Getty Center Dr, Los Angeles, CA 90049, USA",
                        City = LA
                    };
                    var metropolitan = new Venue
                    {
                        Name = "Metropolitan Museum of Art",
                        Address = "1000 5th Ave, New York, NY 10028, USA",
                        City = NY
                    };
                    var madison = new Venue
                    {
                        Name = "Madison Square Garden",
                        Address = "7th Ave & 32nd St, 10001 Manhattan, NY, US",
                        City = NY
                    };
                    var teatrEstrady = new Venue
                    {
                        Name = "Театр эстрады",
                        Address = "Берсеневская наб., 20/2 м.Боровицкая ",
                        City = moscow
                    };
                    var movieMoscow = new Venue
                    {
                        Name = "5 звезд на Новокузнецкой",
                        Address = "Б.Овчинниковский пер., 16, ТЦ «Аркадия» м.Новокузнецкая, Третьяковская ",
                        CityId = moscow.Id
                    };
                    var kzAvrora = new Venue
                    {
                        Name = "Концертный зал Аврора",
                        Address = "Пироговская наб., 5/2 м.Площадь Ленина ",
                        City = peter
                    };
                    var hermitage = new Venue
                    {
                        Name = "Эрмитаж",
                        Address = "Дворцовая пл., 2",
                        City = peter
                    };
                    var minskArena = new Venue
                    {
                        Name = "Минск-Арена",
                        Address = "пр-т Победителей, 111",
                        City = minsk
                    };
                    var nrg = new Venue
                    {
                        Name = "NRG Park",
                        Address = "8400 Kirby Drive, 77054, Houston, TX, US",
                        City = houston
                    };
                    context.Venues.AddRange(
                        kvartirnik,
                        gck,
                        kupalauski,
                        kzMinsk,
                        universalStudios,
                        getty,
                        metropolitan,
                        madison,
                        teatrEstrady,
                        movieMoscow,
                        kzAvrora,
                        hermitage,
                        minskArena,
                        hollywoodBowl,
                        hydePark,
                        nrg
                    );

                    // Events
                    var u2 = new Event
                    {
                        Image = "/images/u2.jpg",
                        Date = new DateTime(2017, 5, 24, 20, 0, 0),
                        Description = "<p>U2 (formed in 1976) is a highly-successful Irish rock band, formed of Bono, The Edge, Larry Mullen, Jr. and Adam Clayton, hailing from Dublin, Ireland.</p>",
                        Name = "U2",
                        Url = "https://www.songkick.com/concerts/28947944-u2-at-nrg-park",
                        Venue = nrg
                    };
                    var killers = new Event
                    {
                        Image = "/images/barclaycard.jpg",
                        Date = new DateTime(2017, 7, 8, 20, 0, 0),
                        Description = "<p>The Killers are an American post-punk alternative rock band formed in Las Vegas, US, in 2001 by members Brandon Flowers and Dave Keuning, who found each other through a newspaper ad.</p>",
                        Name = "Barclaycard British Summer Time 2017",
                        Url = "https://www.songkick.com/festivals/910659-barclaycard-british-summer-time/id/29108514-barclaycard-british-summer-time-2017",
                        Venue = hydePark
                    };
                    var tt34 = new Event
                    {
                        Image = "/images/tt-34-494472.jpg",
                        Date = new DateTime(2016, 12, 10, 18, 0, 0),
                        Description = "<p><strong>Ацетилен</strong></p>",
                        Name = "TT'34",
                        Url = "http://afisha.tut.by/concert-gomel/gomel_tt34/",
                        Venue = gck
                    };
                    var clapton = new Event
                    {
                        Image = "/images/eric_clapton.jpg",
                        Date = new DateTime(2017, 3, 19, 19, 30, 0),
                        Description = @"<p><em>Legendary British guitarist, Eric Clapton (born March 30th, 1945) 
                                    has forged an extraordinary career lasting over six decades, playing 
                                    with some of the most influential groups of the 60s and 70s before 
                                    establishing his talents as a solo artist.</em></p>",
                        Name = "Eric Clapton",
                        Url = "https://www.songkick.com/concerts/28657314-eric-clapton-at-madison-square-garden",
                        Venue = madison
                    };
                    var islamicArts = new Event
                    {
                        Image = "/images/islamic_art.jpg",
                        Date = new DateTime(2017, 1, 21, 10, 30, 0),
                        Description = @"<p>A tour of the new galleries for the Art of the Arab Lands, Turkey, 
                                    Iran, Central Asia, and Later South Asia explores the <a href='www.metmuseum.org'
                                    target='_blank'>
                                    Metropolitan Museum</a>'s collection of Islamic art, a collection that is one of the 
                                    finest and most comprehensive in the world. Fifteen galleries grouped 
                                    by geographic region trace the course of Islamic civilization from Spain 
                                    in the West to India in the East. The tour draws on this collection to 
                                    explore the rich artistic traditions of the Islamic world and the distinct 
                                    cultures within its fold.</p>",
                        Name = "Arts of the Islamic World",
                        Url = "http://www.metmuseum.org/events/programs/met-tours/guided-tours/arts-of-the-islamic-world",
                        Venue = metropolitan
                    };
                    var javier = new Event
                    {
                        Image = "/images/javier.jpg",
                        Date = new DateTime(2016, 12, 29, 22, 45, 0),
                        Description = @"<p>Tour name: Delicious Tour</p>",
                        Name = "Javier Rodríguez Macpherson",
                        Url = "https://www.songkick.com/concerts/28685289-javier-rodriguez-macpherson-at-madison-square-garden",
                        Venue = madison
                    };

                    var monaspectakl = new Event
                    {
                        Image = "/images/na-belarus_-bog-zhyve_1.jpg",
                        Date = new DateTime(2016, 12, 12, 19, 0, 0),
                        Description = @"<p>монаспектакль паводле твораў Уладзіміра Караткевіча</p>
                                <p>Усю разнастайную палітру абліччаў Караткевічавых герояў 
                                увасобіць на сцэне артыст Нацыянальнага акадэмічнага драматычнага 
                                тэатра імя Максіма Горкага Валерый Шушкевіч</p>",
                        Name = "На Беларусі Бог жыве",
                        Url = "http://kupalauski.by/performances/small_stage/on-god-long-live-belarus/",
                        Venue = kupalauski
                    };
                    var brutto = new Event
                    {
                        Image = "/images/sergey-mikhalok-i-gruppa-brutto-7190625.jpg",
                        Date = new DateTime(2017, 3, 8, 19, 0, 0),
                        Description = @"<p><strong>Сергей Михалок и группа BRUTTO собирают «Минск-Арену» 8 марта, 19.00, «Минск-Арена»</strong><br/>
                                    Первый концерт группы BRUTTO в Гомеле собрал поклонников со всей страны: 5000 зрителей приехали, чтобы увидеть первое выступление музыкантов на родной земле. До последнего белорусы не верили, что Сергей Михалок и Ко появятся на сцене, однако состоявшийся концерт доказал обратное: BRUTTO устроили мощное трехчасовое шоу, продемонстрировав отличную форму и новое звучание своих бронебойных хитов.<br/>
                                    В программу «Double Hot» вошли лучшие треки BRUTTO из альбомов «Родны край» и «Underdog», а также золотые хиты «Ляписов» за последние 10 лет. Группа прокатилась с «Double Hot» по городам Украины, Америки и Европы, и, конечно, громко презентовала программу в Гомеле. Уже на родине к BRUTTO присоединилась знаменитая духовая секция «Ляписов» и, безусловно, звучание группы стало еще громче, сочнее и интереснее.<br/>
                                    В сентябре проекту BRUTTO исполнилось 2 года. С первых дней музыканты громко и уверенно заявили о себе и не обманули ожиданий фанатов по всему миру. За эти 2 года BRUTTO выпустили 2 студийных альбома - Underdog и «Родны край», сняли 17 клипов, представили 2 lyric-video, проехали 2 больших тура по Украине в поддержку альбомов «Underdog» и «Родны край», каждый из которых прошел с аншлагом в 30 городах. За это время BRUTTO были хедлайнерами всех крупнейших фестивалей Украины (Z-Games, Фестиваль ZАХІД, Файне Місто, Бандерштат, Схід-Рок, Respublica, PORTO FRANKO GOGOL FEST, Atlas Weekend и др.) и участниками фестивалей в Литве, Польше, Латвии, Молдове и Великобритании.<br/>
                                    <p>",
                        Name = "Группа Brutto ",
                        Url = "http://afisha.tut.by/concert/gruppa_brutto/?rr",
                        Venue = minskArena
                    };
                    context.Events.AddRange(
                        tt34,
                        clapton,
                        islamicArts,
                        javier,
                        monaspectakl,
                        brutto,
                        killers,
                        u2
                    );

                    // Tickets
                    var tt34Ticket1 = new Ticket
                    {
                        Event = tt34,
                        Price = 15,
                        User = admin,
                        State = TicketState.Sold
                    };
                    var tt34Ticket2 = new Ticket
                    {
                        Event = tt34,
                        Price = 25,
                        User = admin,
                        State = TicketState.Sold
                    };
                    var monaspectaklTicket1 = new Ticket
                    {
                        Event = monaspectakl,
                        Price = 5,
                        User = admin,
                        Comment = "Балкон",
                        State = TicketState.ForSale
                    };
                    var claptonTicket1 = new Ticket
                    {
                        Event = clapton,
                        Price = 95,
                        User = user1,
                        State = TicketState.ForSale
                    };
                    var claptonTicket2 = new Ticket
                    {
                        Event = clapton,
                        Price = 400,
                        User = user1,
                        Comment = "Fan zone access",
                        State = TicketState.Sold
                    };
                    var artsTicket1 = new Ticket
                    {
                        Event = islamicArts,
                        Price = 7,
                        User = user1,
                        State = TicketState.Ordered
                    };
                    var artsTicket2 = new Ticket
                    {
                        Event = islamicArts,
                        Price = 7,
                        User = user1,
                        State = TicketState.ForSale
                    };
                    var monaspectaklTicket2 = new Ticket
                    {
                        Event = monaspectakl,
                        Price = 15,
                        User = admin,
                        Comment = "Партэр",
                        State = TicketState.ForSale
                    };
                    var javierTicket1 = new Ticket
                    {
                        Event = javier,
                        Price = 70,
                        User = user1,
                        State = TicketState.ForSale
                    };
                    var javierTicket2 = new Ticket
                    {
                        Event = javier,
                        Price = 70,
                        User = user1,
                        State = TicketState.ForSale
                    };
                    var claptonTicket3 = new Ticket
                    {
                        Event = clapton,
                        Price = 155,
                        User = user1,
                        State = TicketState.ForSale
                    };
                    var claptonTicket4 = new Ticket
                    {
                        Event = clapton,
                        Price = 200,
                        User = user1,
                        Comment = "Close to the stage",
                        State = TicketState.ForSale
                    };
                    var bruttoTicket1 = new Ticket
                    {
                        Event = brutto,
                        Price = 30,
                        User = admin,
                        Comment = "фан-зона",
                        State = TicketState.ForSale
                    };
                    var bruttoTicket2 = new Ticket
                    {
                        Event = brutto,
                        Price = 20,
                        User = admin,
                        Comment = "танцпол",
                        State = TicketState.Ordered
                    };

                    context.Tickets.AddRange(
                        tt34Ticket1,
                        tt34Ticket2,
                        monaspectaklTicket1,
                        monaspectaklTicket2,
                        claptonTicket1,
                        claptonTicket2,
                        claptonTicket3,
                        claptonTicket4,
                        javierTicket1,
                        javierTicket2,
                        artsTicket1,
                        artsTicket2,
                        bruttoTicket1,
                        bruttoTicket2
                    );


                    // Orders
                    context.Orders.AddRange(
                        new Order
                        {
                            User = user2,
                            Ticket = tt34Ticket1,
                            State = OrderState.Confirmed,
                            TrackingNumber = "124312314"
                        },
                        new Order
                        {
                            User = user2,
                            Ticket = tt34Ticket2,
                            State = OrderState.Confirmed,
                            TrackingNumber = "1234123423"
                        },
                        new Order
                        {
                            User = user2,
                            Ticket = artsTicket1,
                            State = OrderState.Pending,
                        },
                        new Order
                        {
                            User = admin,
                            Ticket = claptonTicket2,
                            State = OrderState.Confirmed,
                            TrackingNumber = "31423511243"
                        },
                        new Order
                        {
                            User = user2,
                            Ticket = javierTicket1,
                            State = OrderState.Rejected,
                            RejectReason = "Please email or call me first. I want to make sure you're a real person."
                        },
                        new Order
                        {
                            User = user2,
                            Ticket = bruttoTicket2,
                            State = OrderState.Pending
                        }
                    );

                    context.SaveChanges();
                  
                }
            }
        }

        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }
    }
}
