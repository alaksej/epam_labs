﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public static class DataRepositoryExtensions
    {
        public static IServiceCollection AddInMemoryDataRepository<TDataStore>(this IServiceCollection serviceCollection)
            where TDataStore : EmptyInMemoryDataStore
        {
            serviceCollection.AddScoped<IInMemoryDataStore, TDataStore>();
            serviceCollection.AddSingleton<IGenericRepository<ApplicationUser>, IdInMemoryGenericRepository<string, ApplicationUser>>();
            serviceCollection.AddSingleton<IGenericRepository<ApplicationRole>, IdInMemoryGenericRepository<string, ApplicationRole>>();
            serviceCollection.AddSingleton<IGenericRepository<IdentityUserRole<string>>, InMemoryUserRoleRepository>();
            serviceCollection.AddSingleton<IGenericRepository<IdentityUserToken<string>>, InMemoryUserTokenRepository>();
            serviceCollection.AddSingleton<IGenericRepository<City>, IdInMemoryGenericRepository<int, City>>();
            serviceCollection.AddSingleton<IGenericRepository<Event>, IdInMemoryGenericRepository<int, Event>>();
            serviceCollection.AddSingleton<IGenericRepository<Order>, IdInMemoryGenericRepository<int, Order>>();
            serviceCollection.AddSingleton<IGenericRepository<Ticket>, IdInMemoryGenericRepository<int, Ticket>>();
            serviceCollection.AddSingleton<IGenericRepository<Venue>, IdInMemoryGenericRepository<int, Venue>>();

            return serviceCollection;
        }

        public static IServiceCollection AddEFDataRepository(this IServiceCollection serviceCollection, Action<DbContextOptionsBuilder> optionsBuilder = null)
        {
            serviceCollection.AddDbContext<ApplicationDbContext>(optionsBuilder);
            serviceCollection.AddScoped<IGenericRepository<ApplicationUser>, EFGenericRepository<ApplicationUser>>();
            serviceCollection.AddScoped<IGenericRepository<ApplicationRole>, EFGenericRepository<ApplicationRole>>();
            serviceCollection.AddScoped<IGenericRepository<IdentityUserRole<string>>, EFGenericRepository<IdentityUserRole<string>>>();
            serviceCollection.AddScoped<IGenericRepository<IdentityUserToken<string>>, EFGenericRepository<IdentityUserToken<string>>>();
            serviceCollection.AddScoped<IGenericRepository<City>, EFGenericRepository<City>>();
            serviceCollection.AddScoped<IGenericRepository<Event>, EFGenericRepository<Event>>();
            serviceCollection.AddScoped<IGenericRepository<Order>, EFGenericRepository<Order>>();
            serviceCollection.AddScoped<IGenericRepository<Ticket>, EFGenericRepository<Ticket>>();
            serviceCollection.AddScoped<IGenericRepository<Venue>, EFGenericRepository<Venue>>();

            using (var serviceScope = serviceCollection.BuildServiceProvider().GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().EnsureSeedData();
            }

            return serviceCollection;
        }
    }
}
