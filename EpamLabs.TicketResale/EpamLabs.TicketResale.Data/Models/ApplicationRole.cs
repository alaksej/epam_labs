﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace EpamLabs.TicketResale.Data
{
    public class ApplicationRole : IdentityRole, IEntity<string>
    {
        public ApplicationRole(string roleName) : base(roleName)
        {
        }

        public ApplicationRole() : base(string.Empty)
        {
        }
    }
}
