﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public enum OrderState
    {
        Pending,
        Confirmed,
        Rejected
    }

    public static class OrderStateExtensions
    {
        public static string GetDisplayName(this OrderState state)
        {
            switch (state)
            {
                default:
                    return state.ToString();
            }
        }
    }
}
