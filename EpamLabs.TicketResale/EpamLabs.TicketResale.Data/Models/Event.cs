﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EpamLabs.TicketResale.Data
{
    [DataContract]
    public class Event : Entity<int>
    {
        [DataMember]
        [Required]
        [Display(Name = "Event title")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Date and time")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime Date { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }

        [DataMember]
        [DataType(DataType.Url)]
        [Display(Name = "More info")]
        public string Url { get; set; }

        [DataMember]
        [DataType(DataType.Html)]
        [Display(Name = "Description")]
        [DisplayFormat(HtmlEncode = false)]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Venue")]
        [Required]
        [JsonIgnore]
        public int VenueId { get; set; }

        [DataMember]
        [Display(Name = "Venue")]
        public virtual Venue Venue { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
