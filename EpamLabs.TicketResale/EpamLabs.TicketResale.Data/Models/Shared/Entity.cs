﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EpamLabs.TicketResale.Data
{
    [DataContract]
    public abstract class Entity<TKey> : IEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        [DataMember]
        [Display(AutoGenerateField = false, AutoGenerateFilter = false)]
        public virtual TKey Id { get; set; }
    }
}
