﻿using System;

namespace EpamLabs.TicketResale.Data
{
    public interface IEntity<TKey> where TKey : IEquatable<TKey>
    {
        TKey Id { get; set; }
    }
}
