﻿using System.ComponentModel.DataAnnotations;

namespace EpamLabs.TicketResale.Data
{
    public class Order : Entity<int>
    {
        [Display(Name = "Order status")]
        [Required]
        public OrderState State { get; set; }
        [Display(Name = "Tracking number")]
        public string TrackingNumber { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public int TicketId { get; set; }
        public string RejectReason { get; set; }

        [Display(Name = "Buyer")]
        public virtual ApplicationUser User { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
