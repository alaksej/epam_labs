﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EpamLabs.TicketResale.Data
{
    [DataContract]
    public class Venue : Entity<int>
    {
        [DataMember]
        [Display(Name = "Venue")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Venue address")]
        public string Address { get; set; }

        [JsonIgnore]
        public int CityId { get; set; }

        [DataMember]
        [Display(Name = "City")]
        public virtual City City { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<Event> Events { get; set; }
    }
}
