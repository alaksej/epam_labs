﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EpamLabs.TicketResale.Data
{
    [DataContract]
    public class City : Entity<int>
    {
        [DataMember]
        [Required]
        [MaxLength(100)]
        [Display(Name = "City")]
        public string Name { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<Venue> Venues { get; set; }
    }

}
