﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    public enum TicketState
    {
        ForSale,
        Ordered,
        Sold
    }

    public static class TicketStateExtensions
    {
        public static string GetDisplayName(this TicketState state)
        {
            switch (state)
            {
                case TicketState.ForSale:
                    return "For sale";
                default:
                    return state.ToString();
            }
        }
    }
}
