﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Data
{
    [DataContract]
    public class Page<T>
    {
        public Page() { }

        public Page(IQueryable<T> source, int? page, int? size)
        {
            TotalElements = source.Count();
            Size = size ?? TotalElements;
            TotalPages = Size != 0 ? (int)Math.Ceiling(TotalElements / (double)Size) : 0;
            PageIndex = page ?? 0;
            PageIndex = TotalPages == 0 ? 0 : PageIndex >= TotalPages ? TotalPages - 1 : PageIndex;
            Content = TotalElements > 0 ? source.Skip(PageIndex * Size).Take(Size).ToList() : default(IList<T>);
        }

        public Page(IEnumerable<T> content, int pageIndex, int size, int totalElements)
        {
            Content = content;
            PageIndex = pageIndex;
            TotalElements = totalElements;
        }

        [DataMember]
        public IEnumerable<T> Content { get; set; }

        [DataMember]
        public bool First => (PageIndex == 0);

        [DataMember]
        public bool Last => (TotalPages == 0 || PageIndex == TotalPages - 1);

        [DataMember]
        [Newtonsoft.Json.JsonProperty(PropertyName = "page")]
        public int PageIndex { get; set; }

        [DataMember]
        public int Size { get; set; }

        [DataMember]
        public int Elements => Content?.Count() ?? 0;

        [DataMember]
        public int TotalElements { get; set; }

        [DataMember]
        public int TotalPages { get; set; }
    }
}
