﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace EpamLabs.TicketResale.Data
{
    public class Ticket : Entity<int>
    {
        [Display(Name = "Price")]
        [Required]
        public decimal Price { get; set; }

        [Display(Name = "Seller's notes")]
        public string Comment { get; set; }

        [Display(Name = "Ticket category")]
        [Required]
        public TicketState State { get; set; }

        public int EventId { get; set; }

        [Display(Name ="Seller")]
        [Required]
        public string UserId { get; set; }

        [Display(Name = "Event")]
        public virtual Event Event { get; set; }

        [Display(Name = "Seller")]
        public virtual ApplicationUser User { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
