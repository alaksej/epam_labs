﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace EpamLabs.TicketResale.Data
{
    public class ApplicationUser : IdentityUser, IEntity<string>
    {
        [MaxLength(50)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [MaxLength(50)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name ="Language")]
        public string Locale { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [MaxLength(25)]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public override string Email { get; set; }

        [MaxLength(25)]
        [Display(Name = "Phone")]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        public override string PhoneNumber { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
