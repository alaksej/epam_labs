﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EpamLabs.TicketResale.Tests
{
    public class CitiesControllerTest
    {
        private Mock<ICityService> _service;
        private CitiesController _controller;
        private SampleData _data;

        public CitiesControllerTest()
        {
            _service = new Mock<ICityService>();
            _controller = new CitiesController(new LoggerFactory(), _service.Object);
            _data = new SampleData();
        }

        [Fact]
        public void GetAll_CorrectNumberAndOrder()
        {
            _service.Setup(x => x.GetAll()).Returns(_data.Cities);
            var result = ((_controller.Index() as ViewResult).Model as IEnumerable<City>).ToList();

            Assert.Equal(6, result.Count());
            Assert.Equal("Gomel", result[0].Name);
            Assert.Equal("Los Angeles", result[1].Name);
            Assert.Equal("Minsk", result[2].Name);
            Assert.Equal("Moscow", result[3].Name);
            Assert.Equal("New York", result[4].Name);
            Assert.Equal("St. Petersburg", result[5].Name);
        }

        [Fact]
        public void CreateCity_ValidModel_Redirect()
        {
            var c = new City() { Id = 100, Name = "test_city" };
            var result = (RedirectToActionResult)_controller.Create(c);

            _service.Verify(m => m.Create(c), Times.Once);
            Assert.NotNull(result);
            Assert.Equal("Index", result.ActionName);
        }

        [Fact]
        public void CreateCity_InvalidModel_ViewResult()
        {
            City c = new City { Name = "" };
            _controller.ModelState.AddModelError("Error", "Something went wrong");

            var result = (ViewResult)_controller.Create(c);

            _service.Verify(m => m.Create(c), Times.Never);
            Assert.NotNull(result);
            Assert.Null(result.ViewName);
        }
    }
}
