﻿using EpamLabs.TicketResale.Data;
using System.Linq;
using Xunit;

namespace EpamLabs.TicketResale.Tests.Data
{
    public class PageTest
    {
        private readonly IQueryable<object> _queryable;
        private readonly IQueryable<object> _empty;

        public PageTest()
        {
            _queryable = new[] 
            {
                new { Number = 1 },
                new { Number = 2 },
                new { Number = 3 },
                new { Number = 4 },
                new { Number = 5 },
                new { Number = 6 },
                new { Number = 7 }
            }.AsQueryable();
            _empty = new object[0].AsQueryable();
        }

        [Theory]
        [InlineData(0, 10, true, true, 7, 1, 7)]
        [InlineData(null, null, true, true, 7, 1, 7)]
        [InlineData(0, 2, true, false, 7, 4, 2)]
        [InlineData(null, 2, true, false, 7, 4, 2)]
        [InlineData(1, 2, false, false, 7, 4, 2)]
        [InlineData(3, 2, false, true, 7, 4, 1)]
        [InlineData(4, 2, false, true, 7, 4, 1)]
        public void CreatePageWithSpecifiedParameters(int? page, int? size, bool expectedFirst, bool expectedLast, 
            int expectedTotalElements, int expectedTotalPages, int expectedContentLength)
        {
            /* Arrange */
            /* Act */
            var result = new Page<object>(_queryable, page, size);

            /* Assert */
            Assert.Equal(expectedFirst, result.First);
            Assert.Equal(expectedLast, result.Last);
            Assert.Equal(expectedTotalElements, result.TotalElements);
            Assert.Equal(expectedTotalPages, result.TotalPages);
            Assert.Equal(expectedContentLength, result.Content.Count());
        }

        [Theory]
        [InlineData(0, 10, true, true, 0, 0, 0)]
        [InlineData(null, null, true, true, 0, 0, 0)]
        [InlineData(0, 2, true, true, 0, 0, 0)]
        [InlineData(null, 2, true, true, 0, 0, 0)]
        [InlineData(1, 2, true, true, 0, 0, 0)]
        [InlineData(3, 2, true, true, 0, 0, 0)]
        [InlineData(4, 2, true, true, 0, 0, 0)]
        public void CreatePageWithSpecifiedParameters_EmptyQueryable(int? page, int? size, bool expectedFirst, bool expectedLast,
            int expectedTotalElements, int expectedTotalPages, int expectedContentLength)
        {
            /* Arrange */
            /* Act */
            var result = new Page<object>(_empty, page, size);

            /* Assert */
            Assert.Equal(expectedFirst, result.First);
            Assert.Equal(expectedLast, result.Last);
            Assert.Equal(expectedTotalElements, result.TotalElements);
            Assert.Equal(expectedTotalPages, result.TotalPages);
            Assert.Null(result.Content);
        }
    }
}
