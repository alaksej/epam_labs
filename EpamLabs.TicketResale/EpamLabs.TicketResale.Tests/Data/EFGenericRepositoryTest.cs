﻿using EpamLabs.TicketResale.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EpamLabs.TicketResale.Tests
{
    public class EFGenericRepositoryTests : IDisposable
    {
        private EFGenericRepository<Event> _repository;
        private ApplicationDbContext _context;

        public EFGenericRepositoryTests()
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseInMemoryDatabase();
            var options = builder.Options;

            using (var context = new ApplicationDbContext(options))
            {
                var events = new List<Event>
                {
                    new Event { Id = 1, Name = "Event1", Date = DateTime.Now - TimeSpan.FromDays(2), VenueId = 1 },
                    new Event { Id = 2, Name = "Event2", Date = DateTime.Now - TimeSpan.FromDays(1), VenueId = 1 },
                    new Event { Id = 3, Name = "Event3", Date = DateTime.Now + TimeSpan.FromDays(0), VenueId = 1 },
                    new Event { Id = 4, Name = "Event4", Date = DateTime.Now + TimeSpan.FromDays(1), VenueId = 1 },
                    new Event { Id = 5, Name = "Event5", Date = DateTime.Now + TimeSpan.FromDays(2), VenueId = 1 },
                };

                context.AddRange(events);
                context.SaveChanges();
            }

            _context = new ApplicationDbContext(options);
            _repository = new EFGenericRepository<Event>(_context);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public void GetById_RetrieveCorrectEntityName()
        {
            var name = _repository.GetById(1).Name;
            Assert.Equal("Event1", name);
        }

        [Fact]
        public void GetById_NonExistingId_NullReturned()
        {
            var entity = _repository.GetById(0);
            Assert.Null(entity);
        }

        [Fact]
        public void GetAll_RetrieveAllEntites()
        {
            var entities = _repository.GetAll();
            Assert.NotNull(entities);
            Assert.Equal(5, entities.Count());
        }

        public static IEnumerable<object[]> SearchCriteriaForExistingItems
        {
            get
            {
                return new[]
                {
                    new object[]
                    {
                        "Event4",
                        DateTime.Now + TimeSpan.FromMinutes(1),
                        1,
                        "Event4"
                    },
                    new object[]
                    {
                        "Ev",
                        DateTime.Now - TimeSpan.FromDays(10),
                        5,
                        "Event1Event2Event3Event4Event5"
                    },
                    new object[]
                    {
                        "",
                        DateTime.Now + TimeSpan.FromDays(1.5),
                        1,
                        "Event5"
                    }
                };
            }
        }

        // Use 'dotnet test' from command line
        [Theory]
        [MemberData(nameof(SearchCriteriaForExistingItems))]
        public void Find_SearchByNameAndDate_CorrectCountAndNames(string startsWith, DateTime after, int resultCount, string resultName)
        {
            var data = _repository.Find(x => x.Name.StartsWith(startsWith) && x.Date > after);
            string aggregateName = null;
            foreach (var item in data)
            {
                aggregateName += item.Name;
            }

            Assert.NotNull(data);
            Assert.Equal(data.Count(), resultCount);
            Assert.Equal(aggregateName, resultName);
        }

        [Fact]
        public void CanAddItem()
        {
            var itemToAdd = new Event { Id = 6, Name = "Event6", Date = DateTime.Now, VenueId = 2 };
            var result = _repository.Add(itemToAdd);
            var retrieved = _context.Events.Find(itemToAdd.Id);

            Assert.NotNull(result);
            Assert.NotNull(retrieved);
            Assert.Equal("Event6", retrieved.Name);
        }

        [Fact]
        public void CanUpdateItem()
        {
            var itemToUpdate = _context.Events.Find(1);
            var newName = "UpdatedEvent1";
            itemToUpdate.Name = newName;
            var returnedItem = _repository.Update(itemToUpdate);
            var updatedItem = _context.Events.Find(1);

            Assert.NotNull(returnedItem);
            Assert.Equal(newName, updatedItem.Name);
        }

        [Fact]
        public void CanDeleteItem()
        {
            var itemToDelete = _context.Events.Find(1);
            var returnedItem = _repository.Delete(itemToDelete);
            var deletedItem = _context.Events.Find(1);

            Assert.NotNull(returnedItem);
            Assert.Null(deletedItem);
        }
    }
}
