﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Business.Infrastructure;
using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace EpamLabs.TicketResale.Tests
{
    public class EventServiceTest
    {
        private Mock<IGenericRepository<Event>> _mockEventRepo;
        private Mock<IGenericRepository<Ticket>> _mockTicketRepo;
        private Mock<IGenericRepository<Venue>> _mockVenueRepo;
        private Mock<IGenericRepository<City>> _mockCityRepo;
        private Mock<IGenericRepository<ApplicationUser>> _mockUserRepo;
        private Mock<ISearchService> _mockSearchService;
        private IEventService _service;
        private SampleData _data;

        public EventServiceTest()
        {
            _mockEventRepo = new Mock<IGenericRepository<Event>>();
            _mockTicketRepo = new Mock<IGenericRepository<Ticket>>();
            _mockVenueRepo = new Mock<IGenericRepository<Venue>>();
            _mockCityRepo = new Mock<IGenericRepository<City>>();
            _mockUserRepo = new Mock<IGenericRepository<ApplicationUser>>();
            _mockSearchService = new Mock<ISearchService>();
            _service = new EventService(_mockCityRepo.Object, _mockEventRepo.Object, _mockTicketRepo.Object, _mockVenueRepo.Object, _mockUserRepo.Object, _mockSearchService.Object, new FileService());
            _data = new SampleData();
        }

        [Fact]
        public void GetAll_CorrectNumberOfItems()
        {
            _mockEventRepo.Setup(x => x.GetAll()).Returns(_data.Events);
            _mockTicketRepo.Setup(x => x.GetAll()).Returns(_data.Tickets);
            _mockVenueRepo.Setup(x => x.GetAll()).Returns(_data.Venues);
            _mockCityRepo.Setup(x => x.GetAll()).Returns(_data.Cities);
            _mockUserRepo.Setup(x => x.GetAll()).Returns(_data.Users);

            var results = _service.GetAll();

            Assert.NotNull(results);
            Assert.Equal(6, results.Count());
        }

        [Fact]
        public void CanAddItem_NewItem_Success()
        {
            var newEvent = new Event { Id = 7, Name = "Event7" };
            _mockEventRepo.Setup(x => x.Add(newEvent)).Returns(newEvent);

            _service.Create(newEvent);
            _mockEventRepo.Verify(x => x.Add(newEvent), Times.Once);
        }

        [Fact]
        public void CanAddItem_ExistingItem_Failure()
        {
            var newEvent = new Event { Id = 5, Name = "Event5" };
            _mockEventRepo.Setup(x => x.Add(newEvent)).Returns(newEvent);
            _mockEventRepo.Setup(x => x.GetById(5)).Returns(newEvent);

            Assert.Throws<ServiceException>(() => _service.Create(newEvent));
        }

        [Fact]
        public void CanAddItem_NullItem_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => _service.Create(null));
        }

        [Fact]
        public void GetUpcomingEvents()
        {
            var events = new List<Event>
            {
                new Event { Id = 1, Name = "Event1", Date = DateTime.Now + TimeSpan.FromDays(-2) },
                new Event { Id = 2, Name = "Event2", Date = DateTime.Now + TimeSpan.FromDays(-1) },
                new Event { Id = 3, Name = "Event3", Date = DateTime.Now + TimeSpan.FromDays(1) },
                new Event { Id = 4, Name = "Event4", Date = DateTime.Now + TimeSpan.FromDays(2) },
                new Event { Id = 5, Name = "Event5", Date = DateTime.Now + TimeSpan.FromDays(3) },
            };
            _mockEventRepo.Setup(x => x.GetQueryable())
                .Returns(events.AsQueryable());

            var results = _service.GetUpcoming(0, 10);

            Assert.NotNull(results);
            Assert.Equal(3, results.Content.Count());
        }
    }
}
