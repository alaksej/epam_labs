﻿(function () {
    $("#selectLanguage select").change(function () {
        $(this).parent().submit();
    });

    // Datepicker
    $('#rangestart').calendar({
        type: 'date',
        endCalendar: $('#rangeend')
    });
    $('#rangeend').calendar({
        type: 'date',
        startCalendar: $('#rangestart')
    });

}());


