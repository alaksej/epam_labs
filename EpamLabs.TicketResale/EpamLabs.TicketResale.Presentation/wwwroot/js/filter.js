﻿$(function () {
    var citiesSelect = document.getElementById("cities-filter");
    $.ajax({
        method: 'GET',
        url: '/api/cities',
        data: {
            size: 1000,
            sortBy: 'asc'
        },
        success: function (citiesPage) {
            for (var i = 0; i < citiesPage.totalElements; i++) {
                var opt = citiesPage.content[i];
                var el = document.createElement("option");
                el.textContent = opt.name;
                el.value = opt.id;
                citiesSelect.appendChild(el);
            }
        }
    });

    venuesSelect = document.getElementById("venues-filter");
    $.ajax({
        method: 'GET',
        url: '/api/venues',
        data: {
            size: 1000
        },
        success: function (venuesPage) {
            for (var i = 0; i < venuesPage.totalElements; i++) {
                var opt = venuesPage.content[i];
                var el = document.createElement("option");
                el.textContent = opt.name;
                el.value = opt.id;
                venuesSelect.appendChild(el);
            }
        }
    });

    var $filterInput = $('#filter-name-input');
    $filterInput.keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            applyFilter();
        }
    });

    $filterInput.on('focus', function () {
        this.select();
    });

    var formData;
    $('body').append("<div id='filterPageNum' style='display: none'>0</div>");

    var filterPageNum = $('#filterPageNum');

    $('#apply-filter').on('click', applyFilter);
    $('#reset-filter').on('click', function () {
        $('#filter-form .dropdown.example a').remove();
    });


    var $showMore = $('#show-more');
    $showMore.css('display', 'none');

    $showMore.on("click", function () {
        var pageNum = filterPageNum.html();
        pageNum++;
        filterPageNum.html(pageNum);
        formData = formData.replace('page=' + (pageNum - 1), 'page=' + pageNum);
        $.ajax({
            method: 'GET',
            url: 'api/events',
            data: formData,
            success: function (anotherPage) {
                addAnotherPage(anotherPage);
            }
        });
    });

    function applyFilter() {
        $('#suggestion-tips').remove();
        filterPageNum.html(0);
        var form = $('#filter-form');
        formData = form.serialize();
        formData = formData + "&size=3&page=0";
        $.ajax({
            method: 'GET',
            url: 'api/events',
            data: formData,
            success: function (page) {
                renderResults(page);
            }
        });
    }

    function renderResults(page) {
        $('.pagination-wrapper').css('display', 'none');
        var heading = document.getElementById('events-heading');
        var container = $('#events-container');
        $('#events-container .event-content').remove();
        if (page.totalElements == 0) {
            heading.textContent = "Your search did not match any documents";
            container.append("<div id='suggestion-tips'>Suggestions:<br/><ul>"
                + "<li>Make sure all the words are spelled correctly</li>"
                + "<li>Try fewer search conditions</ul></div>");
            return;
        }
        heading.textContent = page.totalElements + " event(s) found:";
        page.content.forEach(function (event) {
            renderEvent(event);
        });
        if (page.last) return;
        
        $showMore.css('display', 'block');
        
    }

    function addAnotherPage(anotherPage) {

        anotherPage.content.forEach(function (event) {
            renderEvent(event);
        });
        if (anotherPage.last) {
            $showMore.css('display', 'none');
        }
    }

    function renderEvent(event) {
        var container = $('#events-container');
        var div = $('<div class="event-content">');
        var right = $('<div class="right">');
        var a = $('<a href="Events/Details/' + event.id + '">');
        a.append('<img class="center" src=' + event.image + ' />');
        right.append(a);
        div.append(right);
        var desc = $('<div class="event-description">');
        var h3 = $('<h3>');
        var aDetails = $('<a href="Events/Details/' + event.id + '">' + event.name + '</a>');
        h3.append(aDetails);
        desc.append(h3);
        var date = new Date(event.date);
        var dateString =
            date.getUTCFullYear() + "/" +
            ("0" + (date.getUTCMonth() + 1)).slice(-2) + "/" +
            ("0" + date.getUTCDate()).slice(-2) + " " +
            ("0" + date.getUTCHours()).slice(-2) + ":" +
            ("0" + date.getUTCMinutes()).slice(-2);
        desc.append('<p class="date">' + dateString + '</p>');
        desc.append('<p>' + event.venue.name + ', ' + event.venue.city.name + '</p>');
        div.append(desc);
        div.insertBefore($showMore);
    }
});
