﻿(function () {
    $("input.autocomplete").keyup(function () {
        var self = $(this);
        var text = self.val();
        var names;
        //self.autocomplete();
        $('#filter-name-wrapper').addClass('loading');
        $.ajax({
            type: 'GET',
            url: '/api/events/autocompletion',
            data: {
                q: text
            },
            success: function (events) {
                names = events.map(function (e) {
                    return e.name;
                });
                self.autocomplete({
                    source: names
                });
                $('#filter-name-wrapper').removeClass('loading');
            }

        });
    });
}());

