﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewComponents.Account
{
    public class LoginViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserService _users;

        public LoginViewComponent(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IUserService users)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _users = users;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = HttpContext.User;
            if (_signInManager.IsSignedIn(user))
            {
                var appUser = _users.GetCurrent();
                var displayName = appUser?.FirstName + " " + appUser?.LastName;
                if (string.IsNullOrWhiteSpace(displayName))
                {
                    displayName = _userManager.GetUserName(user);
                }
                return View("LoggedIn", displayName);
            }
            else
            {
                return View();
            }
        } 
    }
}
