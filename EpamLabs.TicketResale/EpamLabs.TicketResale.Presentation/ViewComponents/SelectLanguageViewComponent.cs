﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewComponents.Account
{
    public class SelectLanguageViewComponent : ViewComponent
    {
        private readonly IOptions<RequestLocalizationOptions> _locOptions;

        public SelectLanguageViewComponent(
            IOptions<RequestLocalizationOptions> locOptions)
        {
            _locOptions = locOptions;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var requestCulture = HttpContext.Features.Get<IRequestCultureFeature>();
            ViewData["requestCultureProvider"] = requestCulture?.Provider?.GetType().Name;
            ViewData["requestCulture"] = requestCulture.RequestCulture.UICulture.Name;
            var cultureItems = _locOptions.Value.SupportedUICultures
                .Select(c => new SelectListItem { Value = c.Name, Text = c.DisplayName })
                .ToList();

            return View(cultureItems);
        } 
    }
}
