﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Business.Formatters;
using EpamLabs.TicketResale.Business.Localization;
using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Business.TwoFactorAuth;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;

namespace EpamLabs.TicketResale.Presentation
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddCustomUserStore()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireNonAlphanumeric = Configuration.GetValue<bool>("IdentityOptions:Password:RequireNonAlphanumeric");
                options.Password.RequireUppercase = Configuration.GetValue<bool>("IdentityOptions:Password:RequireUppercase");
                options.Password.RequireDigit = Configuration.GetValue<bool>("IdentityOptions:Password:RequireDigit");
                options.Password.RequireLowercase = Configuration.GetValue<bool>("IdentityOptions:Password:RequireLowercase");
                options.Password.RequiredLength = Configuration.GetValue<int>("IdentityOptions:Password:RequiredLength");
                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromMinutes(Configuration.GetValue<int>("IdentityOptions:Cookies:ApplicationCookie:ExpireTimeSpanInMinutes"));
                options.Cookies.ApplicationCookie.LoginPath = new PathString(Configuration["IdentityOptions:Cookies:ApplicationCookie:LoginPath"]);
                options.Cookies.ApplicationCookie.AccessDeniedPath = new PathString(Configuration["IdentityOptions:Cookies:ApplicationCookie:AccessDeniedPath"]);
                options.Cookies.ApplicationCookie.SlidingExpiration = true;
                options.User.RequireUniqueEmail = false;
            });

            services.Configure<GoogleTranslateOptions>(Configuration.GetSection("GoogleApiSettings"));

            services.AddLocalization();

            services.AddMvc(options =>
            {
                options.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                options.OutputFormatters.Add(new CsvOutputFormatter());
            })
                .AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .AddOnlineLocalization();

            services.AddScoped<ILocalizationService, LocalizationService>();

            services.AddDataServices();

            //services.AddInMemoryDataRepository<SampleInMemoryDataStore>();
            services.AddEFDataRepository(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.Configure<SmsSettings>(Configuration.GetSection("SmsSettings"));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Ticket resale API", Version = "v1", Contact = new Contact { Name = "Aliaksei Yankou", Email = "aliaksei_yankou@epam.com" } });
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "EpamLabs.TicketResale.Presentation.xml");
                c.IncludeXmlComments(xmlPath);
            });

            services.Configure<ElasticsearchOptions>(Configuration.GetSection("Elasticsearch"));
            services.AddElasticSearch();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            
            app.UseStaticFiles();

            app.UseIdentity();

            // thanks to Ashley Lee: http://stackoverflow.com/a/38939050/5983311
            app.UseGetRoutesMiddleware(GetRoutes);

            app.UseCustomRequestLocalization(Configuration["SupportedLanguages"].Split(','));
            app.UseMvc(GetRoutes);
            app.UseSwagger();
            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ticket resale API");
            });
        }

        private readonly Action<IRouteBuilder> GetRoutes =
            routes =>
            {
                routes.MapRoute(
                    name: "custom",
                    template: "{culture:regex(^[a-z]{{2}}$)=en}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            };
    }
}
