﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserService _users;
        private readonly ILogger<UsersController> _logger;

        public UsersController(
            UserManager<ApplicationUser> userManager,
            IUserService users, 
            ILogger<UsersController> logger)
        {
            _userManager = userManager;
            _users = users;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var current = _users.GetCurrent();
            var model = _users.GetAll()
                .Where(x => x.Id != current.Id)
                .Select(x => new UsersViewModel
                {
                    User = x,
                    RoleName = _userManager.GetRolesAsync(x).Result.FirstOrDefault() ?? "User"
                })
                .AsEnumerable()
                .OrderBy(x => x.User.UserName);
            return View(model);
        }

        public IActionResult GrantAdmin(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _users.GetById(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(new UsersViewModel
            {
                User = user,
                RoleName = _userManager.GetRolesAsync(user).Result.FirstOrDefault() ?? "User"
            });
        }

        [HttpPost, ActionName("GrantAdmin")]
        [ValidateAntiForgeryToken]
        public IActionResult GrantAdminConfirmed(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _userManager.FindByIdAsync(id).Result;
            if (user == null)
            {
                return NotFound();
            }
            var result = _userManager.AddToRoleAsync(user, Roles.Admin).Result;
            if (result.Succeeded)
            {
                _logger.LogInformation($"User {user.UserName} has been granted admin rights.");
            }
            else
            {
                _logger.LogInformation($"Unable to grant admin rights to user: {user.UserName}.");
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult RevokeAdmin(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _users.GetById(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(new UsersViewModel
            {
                User = user,
                RoleName = _userManager.GetRolesAsync(user).Result.FirstOrDefault() ?? "User"
            });
        }

        [HttpPost, ActionName("RevokeAdmin")]
        [ValidateAntiForgeryToken]
        public IActionResult RevokeAdminConfirmed(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _userManager.FindByIdAsync(id).Result;
            if (user == null)
            {
                return NotFound();
            }
            var result = _userManager.RemoveFromRoleAsync(user, Roles.Admin).Result;
            if (result.Succeeded)
            {
                _logger.LogInformation($"Admin rights were revoked for user {user.UserName}.");
            }
            else
            {
                _logger.LogInformation($"Unable to revoke admin rights for user: {user.UserName}.");
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
