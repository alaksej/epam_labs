﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EpamLabs.TicketResale.Presentation.Controllers.Api
{
    [Route("api/[controller]")]
    public class CitiesController : Controller
    {
        private readonly ICityService _cities;

        public CitiesController(ICityService cities)
        {
            _cities = cities;
        }

        // GET api/cities?q=gom
        /// <summary>
        /// Gets the list of cities
        /// </summary>
        /// <param name="q">Query to look up the city name</param>
        /// <param name="sortBy">Sort by name: "asc" or "desc"</param>
        /// <param name="page">Page number</param>
        /// <param name="size">Number of elements per page</param>
        /// <returns>Page of cities</returns>
        [HttpGet]
        [ProducesResponseType(typeof(Page<City>), 200)]
        public IActionResult Search(
            [FromQuery] string q,
            [FromQuery] string sortBy,
            [FromQuery] int? page = 0,
            [FromQuery] int? size = 10)
        {
            return Ok(_cities.Search(q, sortBy, page, size));
        }
    }
}
