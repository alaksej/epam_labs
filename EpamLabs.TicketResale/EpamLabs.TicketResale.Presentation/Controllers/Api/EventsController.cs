﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace EpamLabs.TicketResale.Presentation.Controllers.Api
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {
        private readonly IEventService _events;

        public EventsController(IEventService events)
        {
            _events = events;
        }

        /// <summary>
        /// Searches for the events
        /// </summary>
        /// <param name="q">Query to look up the event name</param>
        /// <param name="since">Earliest date of the event, e.g. 2017-02-19T19:13:03.869Z</param>
        /// <param name="until">Latest date of the event, e.g. 2018-02-19T19:13:03.869Z</param>
        /// <param name="venueIds">Ids of events' venues</param>
        /// <param name="cityIds">Ids of events' cities</param>
        /// <param name="sortBy">Sort:"name_asc", "name_desc", "date_asc", "date_desc"</param>
        /// <param name="page">Page number. By default: 0.</param>
        /// <param name="size">Number of elements per page. By default: 5.</param>
        /// <returns>Page of venues</returns>
        [HttpGet]
        [ProducesResponseType(typeof(Page<Event>), 200)]
        public IActionResult Search(
            [FromQuery] string q,
            [FromQuery] DateTime since,
            [FromQuery] DateTime until,
            [FromQuery] int[] venueIds,
            [FromQuery] int[] cityIds,
            [FromQuery] string sortBy = "date_asc",
            [FromQuery] int? page = 0,
            [FromQuery] int? size = 5)
        {
            var result = _events.Query(q, since, until, venueIds, cityIds, sortBy, page, size);
            return Ok(result);
        }

        /// <summary>
        /// Searches for events by name and returns a list of most relevant search results .
        /// </summary>
        /// <param name="q">Search query, e.g. "ar"</param>
        /// <returns><see cref="IEnumerable{Event}"/> of 10 most relevant results.</returns>
        [HttpGet("autocompletion")]
        [ProducesResponseType(typeof(IEnumerable<Event>), 200)]
        public IActionResult Autocompletion([FromQuery] string q)
        {
            var results = _events.SuggestCompletions(q);
            return Ok(results);
        }
    }
}
