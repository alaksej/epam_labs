﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Manage;
using EpamLabs.TicketResale.Presentation.ViewModels.UserInfo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize]
    public class UserInfoController : Controller
    {
        private readonly IUserService _users;
        private readonly ITicketService _tickets;
        private readonly IEventService _events;
        private readonly IOrderService _orders;
        private readonly ILogger _logger;
        private readonly IOptions<RequestLocalizationOptions> _locOptions;

        public UserInfoController(
            IUserService users,
            ITicketService tickets,
            IOrderService orders,
            IEventService events,
            ILoggerFactory loggerFactory,
            IOptions<RequestLocalizationOptions> locOptions)
        {
            _users = users;
            _tickets = tickets;
            _orders = orders;
            _events = events;
            _logger = loggerFactory.CreateLogger<EventsController>();
            _locOptions = locOptions;
        }

        public IActionResult Index(ManageMessage message = ManageMessage.None)
        {
            var user = _users.GetCurrent();
            if (user == null)
            {
                _logger.LogWarning("Unable to get current user info");
                return NotFound();
            }
            return View(new UserInfoIndexViewModel { User = user, Message = message });
        }

        public IActionResult Edit()
        {
            ViewBag.CultureItems = _locOptions.Value.SupportedUICultures
                .Select(c => new SelectListItem { Value = c.Name, Text = c.DisplayName })
                .ToList();
            return View(_users.GetCurrent());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ApplicationUser model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _users.UpdateUserInfo(model);
                    return RedirectToAction(nameof(Index));
                }
                catch (UnauthorizedAccessException)
                {
                    _logger.LogWarning("Unauthorized attempt to modify user data.");
                    return NotFound();
                }
            }
            return View(model);
        }
    }

}
