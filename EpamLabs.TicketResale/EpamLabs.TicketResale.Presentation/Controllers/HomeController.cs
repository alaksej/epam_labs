﻿using EpamLabs.TicketResale.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEventService _eventService;
        private readonly IUserService _userService;
        private readonly ILogger<HomeController> _logger;
        private readonly ILocalizationService _localizationService;

        public HomeController(
            IEventService eventService,
            IUserService userService,
            ILoggerFactory loggerFactory,
            ILocalizationService localizationService)
        {
            _eventService = eventService;
            _userService = userService;
            _logger = loggerFactory.CreateLogger<HomeController>();
            _localizationService = localizationService;
        }

        public IActionResult Index(int page = 0, int size = 3)
        {
            return View(_eventService.GetUpcoming(page, size));
        }

        public IActionResult SetCulture(string culture, string returnUrl)
        {
            _userService.UpdateUserCulture(culture);
            _localizationService.SetCultureCookie(culture);
            returnUrl = _localizationService.AdaptUrlToCulture(returnUrl, culture);
            return RedirectToLocal(returnUrl);
        }

        public IActionResult Error()
        {
            return View();
        }

        #region Helpers

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(EventsController.Index),
                nameof(EventsController).Replace("Controller", ""));
            }
        }

        #endregion
    }
}
