﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Events;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventsController : Controller
    {
        private const string IMAGES_SUBFOLDER = "images";

        private readonly IEventService _events;
        private readonly ITicketService _tickets;
        private readonly IVenueService _venues;
        private readonly IUserService _users;
        private readonly FileService _fileService;
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _env;

        public EventsController(
            IEventService events,
            ITicketService tickets,
            IVenueService venues,
            IUserService users,
            ILoggerFactory loggerFactory,
            FileService fileService,
            IHostingEnvironment env)
        {
            _events = events;
            _tickets = tickets;
            _venues = venues;
            _users = users;
            _fileService = fileService;
            _logger = loggerFactory.CreateLogger<EventsController>();
            _env = env;
        }

        public IActionResult Index()
        {
            return View(_events.GetAll());
        }

        [AllowAnonymous]
        public IActionResult Details(int? id)
        {
            if (id == null)
                return NotFound();
            var model = new EventDetailsViewModel();
            model.Event = _events.GetById((int)id);
            if (model.Event == null)
            {
                _logger.LogWarning($"Event with id={id} not found.");
                return NotFound();
            }
            model.TicketsForSale = _tickets.GetForSale(model.Event, _users.GetCurrent());
            return View(model);
        }

        public IActionResult Create()
        {
            ViewBag.Venues = _venues.GetAllAsSelectList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Event model, IFormFile image = null)
        {
            if (ModelState.IsValid)
            {
                model.Image = _events.UploadImage(image, _env.WebRootPath, IMAGES_SUBFOLDER);
                _events.Create(model);
                _logger.LogInformation("A new event created.");
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Venues = _venues.GetAllAsSelectList();
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var @event = _events.GetById((int)id);
            if (@event == null)
            {
                return NotFound();
            }
            ViewBag.Venues = _venues.GetAllAsSelectList();
            return View(@event);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Event model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _events.Update(model);
                _logger.LogInformation("An event was updated.");
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Venues = _venues.GetAllAsSelectList();
            return View(model);
        }

        [HttpPost]
        public IActionResult UpdateImage(int id, IFormFile image = null)
        {
            var @event = _events.UpdateImage(id, image, _env.WebRootPath, IMAGES_SUBFOLDER);
            return RedirectToAction(nameof(Edit), new { id = id });
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = _events.GetById((int)id);
            if (@event == null)
            {
                return NotFound();
            }
            if (!_events.IsSafeToDelete((int)id))
            {
                ViewBag.Deleteable = false;
            }
            else
            {
                ViewBag.Deleteable = true;
            }
            return View(@event);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_events.IsSafeToDelete(id))
            {
                var @event = _events.GetById(id);
                _fileService.RemoveFile(_env.WebRootPath, @event.Image);
                _events.Delete(@event);
            }
            return RedirectToAction("Index");
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(EventsController.Index),
                nameof(EventsController).Replace("Controller", ""));
            }
        }

        #endregion
    }
}
