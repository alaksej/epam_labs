﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Tickets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize]
    public class TicketsController : Controller
    {
        private readonly ILogger<TicketsController> _logger;
        private readonly ITicketService _tickets;
        private readonly IOrderService _orders;
        private readonly IUserService _users;
        private readonly IEventService _events;

        public TicketsController(
            ILoggerFactory loggerFactory,
            ITicketService tickets,
            IOrderService orders,
            IUserService users,
            IEventService events)
        {
            _logger = loggerFactory.CreateLogger<TicketsController>();
            _tickets = tickets;
            _orders = orders;
            _users = users;
            _events = events;
        }

        public IActionResult Index(TicketsIndexMessage message = TicketsIndexMessage.None)
        {
            var user = _users.GetCurrent();
            var model = new TicketsIndexViewModel
            {
                ForSale = _tickets.GetForUser(user, TicketState.ForSale),
                Ordered = _orders.GetForSeller(user, OrderState.Pending),
                Sold = _orders.GetForSeller(user, OrderState.Confirmed),
                Message = message
            };
            return View(model);
        }
        
        public IActionResult Create(int? eventId)
        {
            ViewBag.Events = _events.GetAllAsSelectList();
            return View(new Ticket
            {
                EventId = eventId ?? 0,
                UserId = _users.GetCurrent().Id,
                State = TicketState.ForSale
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Ticket model)
        {
            if (ModelState.IsValid)
            {
                if (model.UserId != _users.GetCurrent().Id
                    || model.State != TicketState.ForSale)
                {
                    return NotFound();
                }
                _tickets.Create(model);
                _logger.LogInformation("A new ticket added.");
                return RedirectToAction(nameof(Index), new { message = TicketsIndexMessage.TicketAdded });
            }
            ViewBag.Events = _events.GetAllAsSelectList();
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ticket = _tickets.GetById((int)id);
            if (ticket == null)
            {
                return NotFound();
            }
            ViewBag.Events = _events.GetAllAsSelectList();
            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Ticket ticket)
        {
            if (id != ticket.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ticket.UserId != _users.GetCurrent().Id)
                {
                    return NotFound();
                }
                _tickets.Update(ticket);
                _logger.LogInformation("A ticket was updated.");
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Events = _events.GetAllAsSelectList();
            return View(ticket);
        }

        public IActionResult Delete(int id)
        {
            var ticket = _tickets.GetById(id);
            ticket.Event = _events.GetById(ticket.EventId);
            if (ticket == null)
            {
                return NotFound();
            }
            return View(ticket);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (_tickets.IsSafeToDelete(id))
            {
                var ticket = _tickets.GetById(id);
                if (ticket == null 
                    || ticket.UserId != _users.GetCurrent().Id)
                {
                    return NotFound();
                }
                _tickets.Delete(ticket);
            }
            else
            {
                _logger.LogWarning("Deleting ticket failed.");
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
