﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Business.Infrastructure;
using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Orders;
using EpamLabs.TicketResale.Presentation.ViewModels.Tickets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly ITicketService _tickets;
        private readonly IUserService _users;
        private readonly IEventService _events;
        private readonly IOrderService _orders;

        public OrdersController(
            ILoggerFactory loggerFactory,
            ITicketService tickets,
            IUserService users,
            IEventService events,
            IOrderService orders)
        {
            _logger = loggerFactory.CreateLogger<OrdersController>();
            _tickets = tickets;
            _users = users;
            _events = events;
            _orders = orders;
        }

        public IActionResult Index(OrdersIndexMessage message = OrdersIndexMessage.None)
        {
            return View(new OrdersIndexViewModel
            {
                Message = message,
                Orders = _orders.GetForBuyer(_users.GetCurrent())
            });
        }
        
        public IActionResult Create(int id, string returnUrl = null)
        {
            var ticket = _tickets.GetById(id);
            if (ticket == null)
            {
                _logger.LogWarning($"Failed to find ticket with Id: {id}.");
                return NotFound();
            }
            ViewBag.ReturnUrl = returnUrl;
            return View(ticket);
        }

        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult CreateConfirmed(int id)
        {
            try
            {
                var user = _users.GetCurrent();
                _orders.PlaceOrder(user, id);
                return RedirectToAction(nameof(Index), new { message = OrdersIndexMessage.OrderPlaced });
            }
            catch (UnauthorizedException)
            {
                return StatusCode(409);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
        
        public IActionResult Cancel(int id)
        {
            var order = _orders.GetById(id);
            if (order == null)
            {
                return NotFound();
            }
            var user = _users.GetCurrent();
            if (user == null || order.User.Id != _users.GetCurrent().Id)
            {
                return StatusCode(409);
            }
            return View(order);
        }

        [HttpPost, ActionName("Cancel")]
        [ValidateAntiForgeryToken]
        public IActionResult CancelConfirmed(int id)
        {
            try
            {
                var user = _users.GetCurrent();
                _orders.CancelOrder(user, id);
                _logger.LogInformation("Order cancelled.");
                return RedirectToAction(nameof(Index), new { message = OrdersIndexMessage.OrderCancelled });
            }
            catch (UnauthorizedException)
            {
                return StatusCode(409);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        public IActionResult Confirm(int id)
        {
            var order = _orders.GetById(id);
            if (order == null)
            {
                return NotFound();
            }
            var user = _users.GetCurrent();
            if (order.Ticket.UserId != user.Id)
            {
                return new StatusCodeResult(409);
            }
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Confirm(Order model)
        {
            var order = _orders.GetById(model.Id);
            if (order == null)
            {
                return NotFound();
            }
            var user = _users.GetCurrent();
            if (order.Ticket.UserId != user.Id)
            {
                _logger.LogWarning("Unauthorized attempt to confirm an order.");
                return StatusCode(409);
            }
            order.TrackingNumber = model.TrackingNumber;
            try
            {
                _orders.ConfirmOrder(user, order);
                _logger.LogInformation("Order confirmed successfully.");
                return RedirectToAction("Index", "Tickets", new { message = TicketsIndexMessage.TicketSaleConfirmed });
            }
            catch (UnauthorizedException)
            {
                return StatusCode(409);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        public IActionResult Reject(int id)
        {
            var order = _orders.GetById(id);
            if (order == null)
            {
                return NotFound();
            }
            var user = _users.GetCurrent();
            if (order.Ticket.UserId != user.Id)
            {
                return StatusCode(409);
            }
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Reject(Order model)
        {
            var order = _orders.GetById(model.Id);
            if (order == null)
            {
                return NotFound();
            }
            var user = _users.GetCurrent();
            if (order.Ticket.UserId != user.Id)
            {
                _logger.LogWarning("Unauthorized attempt to reject an order.");
                return StatusCode(409);
            }
            order.RejectReason = model.RejectReason;
            try
            {
                _orders.RejectOrder(user, order);
                _logger.LogInformation("Order rejected successfully.");
                return RedirectToAction("Index", "Tickets", new { message = TicketsIndexMessage.TicketSaleRejected });
            }
            catch (UnauthorizedException)
            {
                return StatusCode(409);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

    }
}
