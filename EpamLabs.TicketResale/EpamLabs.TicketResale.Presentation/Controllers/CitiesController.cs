﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CitiesController : Controller
    {
        private readonly ILogger _logger;
        private readonly ICityService _cities;

        public CitiesController(
            ILoggerFactory loggerFactory,
            ICityService cities)
        {
            _logger = loggerFactory.CreateLogger<CitiesController>();
            _cities = cities;
        }

        public IActionResult Index()
        {
            return View(_cities.GetAll().OrderBy(x => x.Name));
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(City model)
        {
            if (ModelState.IsValid)
            {
                _cities.Create(model);
                _logger.LogInformation("A new city created.");
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var city = _cities.GetById((int)id);
            if (city == null)
            {
                return NotFound();
            }
            return View(city);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, City model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _cities.Update(model);
                _logger.LogInformation("A city was updated.");
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var city = _cities.GetById((int)id);
            if (city == null)
            {
                return NotFound();
            }

            if (!_cities.IsSafeToDelete((int)id))
            {
                ViewBag.Deleteable = false;
            }
            else
            {
                ViewBag.Deleteable = true;
            }
            return View(city);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_cities.IsSafeToDelete(id))
            {
                var city = _cities.GetById(id);
                _cities.Delete(city);
            }
            return RedirectToAction("Index");
        }
    }
}
