﻿using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VenuesController : Controller
    {
        private readonly ILogger _logger;
        private readonly IVenueService _venues;
        private readonly ICityService _cities;

        public VenuesController(
            ILoggerFactory loggerFactory,
            IVenueService venues,
            ICityService cities)
        {
            _logger = loggerFactory.CreateLogger<VenuesController>();
            _venues = venues;
            _cities = cities;
        }

        public IActionResult Index()
        {
            return View(_venues.GetAll().OrderBy(x => x.Name));
        }

        public IActionResult Create()
        {
            ViewBag.Cities = _cities.GetAllAsSelectList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Venue model)
        {
            if (ModelState.IsValid)
            {
                _venues.Create(model);
                _logger.LogInformation("A new venue created.");
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var venue = _venues.GetById((int)id);
            if (venue == null)
            {
                return NotFound();
            }
            ViewBag.Cities = _cities.GetAllAsSelectList();
            return View(venue);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Venue model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                _venues.Update(model);
                _logger.LogInformation("A city was updated.");
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Cities = _cities.GetAllAsSelectList();
            return View(model);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var venue = _venues.GetById((int)id);
            if (venue == null)
            {
                return NotFound();
            }

            if (!_venues.IsSafeToDelete((int)id))
            {
                ViewBag.Deleteable = false;
            }
            else
            {
                ViewBag.Deleteable = true;
            }
            return View(venue);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_venues.IsSafeToDelete(id))
            {
                var venue = _venues.GetById(id);
                _venues.Delete(venue);
            }
            return RedirectToAction("Index");
        }
    }
}
