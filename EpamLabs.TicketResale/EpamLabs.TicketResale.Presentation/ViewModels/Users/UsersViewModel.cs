﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Users
{
    public class UsersViewModel
    {
        public ApplicationUser User { get; set; }
        public string RoleName { get; set; }
    }
}
