﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Orders
{
    public class OrdersIndexViewModel
    {
        public IEnumerable<Order> Orders { get; set; }
        public OrdersIndexMessage Message { get; set; }
    }

    public enum OrdersIndexMessage
    {
        None,
        OrderPlaced,
        OrderCancelled,
        Error,
        OrderConfirmed
    }

    public static class IndexMessageExtensions
    {
        public static string GetMessage(this OrdersIndexMessage message)
        {
            switch (message)
            {
                case OrdersIndexMessage.OrderPlaced:
                    return "Your order has been placed successfully";
                case OrdersIndexMessage.OrderCancelled:
                    return "Your order has been cancelled";
                case OrdersIndexMessage.Error:
                    return "Error occured. Please try again.";
                case OrdersIndexMessage.OrderConfirmed:
                    return "The order has been confirmed";
                default:
                    return string.Empty;
            }
        }
    }
}
