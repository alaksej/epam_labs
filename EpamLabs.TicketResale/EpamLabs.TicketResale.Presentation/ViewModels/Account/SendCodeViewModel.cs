﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Account
{
    public class SendCodeViewModel
    {
        public string ReturnUrl { get; set; }

        public bool RememberMe { get; set; }
    }
}
