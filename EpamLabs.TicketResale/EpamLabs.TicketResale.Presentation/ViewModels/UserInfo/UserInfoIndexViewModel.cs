﻿using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation.ViewModels.Manage;

namespace EpamLabs.TicketResale.Presentation.ViewModels.UserInfo
{
    public class UserInfoIndexViewModel
    {
        public ApplicationUser User { get; set; }
        public ManageMessage Message { get; set; }
    }
}
