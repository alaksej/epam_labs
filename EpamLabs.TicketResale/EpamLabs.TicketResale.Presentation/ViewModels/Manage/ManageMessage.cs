﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Manage
{
    public enum ManageMessage
    {
        None,
        AddPhoneSuccess,
        AddLoginSuccess,
        ChangePasswordSuccess,
        SetTwoFactorSuccess,
        DisableTwoFactorSuccess,
        SetPasswordSuccess,
        RemoveLoginSuccess,
        RemovePhoneSuccess,
        Error
    }

    public static class ManageMessageExtensions
    {
        public static string GetMessage(this ManageMessage message)
        {
            switch (message)
            {
                case ManageMessage.AddPhoneSuccess:
                    return "Your phone number was added.";
                case ManageMessage.RemovePhoneSuccess:
                    return "Your phone number was removed.";
                case ManageMessage.SetTwoFactorSuccess:
                    return "Two-factor authentication using SMS has been enabled.";
                case ManageMessage.DisableTwoFactorSuccess:
                    return "Two-factor authentication has been disabled.";
                
                default:
                    return message.ToString();
            }
        }
    }
}
