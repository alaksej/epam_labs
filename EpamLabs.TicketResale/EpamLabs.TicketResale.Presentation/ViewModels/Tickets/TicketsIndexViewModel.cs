﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Tickets
{
    public class TicketsIndexViewModel
    {
        public IEnumerable<Ticket> ForSale { get; set; }
        public IEnumerable<Order> Ordered { get; set; }
        public IEnumerable<Order> Sold { get; set; }
        public TicketsIndexMessage Message { get; set; }
    }

    public enum TicketsIndexMessage
    {
        None,
        TicketAdded,
        TicketRemoved,
        TicketSaleConfirmed,
        TicketSaleRejected,
        Error
    }

    public static class IndexMessageExtensions
    {
        public static string GetMessage(this TicketsIndexMessage message)
        {
            switch (message)
            {
                case TicketsIndexMessage.TicketAdded:
                    return "Ticket added to your list for sale";
                case TicketsIndexMessage.TicketRemoved:
                    return "Ticket removed from your list for sale";
                case TicketsIndexMessage.TicketSaleConfirmed:
                    return "Ticket sale confirmed";
                case TicketsIndexMessage.TicketSaleRejected:
                    return "Ticket sale rejected";
                case TicketsIndexMessage.Error:
                    return "Error occured. Please try again.";
                default:
                    return string.Empty;
            }
        }
    }
}
