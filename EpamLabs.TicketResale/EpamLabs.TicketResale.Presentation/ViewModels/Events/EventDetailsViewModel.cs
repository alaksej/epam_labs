﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Presentation.ViewModels.Events
{
    public class EventDetailsViewModel
    {
        public Event Event { get; set; }
        public IEnumerable<Ticket> TicketsForSale { get; set; }
    }
}
