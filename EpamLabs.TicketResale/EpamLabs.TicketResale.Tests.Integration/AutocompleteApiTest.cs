﻿using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace EpamLabs.TicketResale.Tests.Integration
{
    public class AutocompleteApiTest : BaseTest
    {
        [Fact]
        public void GetAll()
        {
            // Act 
            var page = Get<IEnumerable<Event>>("/api/events/autocompletion?q=br");

            // Assert
            Assert.Equal("Группа Brutto ", page.First().Name);
        }
    }
}
