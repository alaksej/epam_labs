﻿using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace EpamLabs.TicketResale.Tests.Integration
{
    public class EventsApiTest : BaseTest
    {
        [Fact]
        public void GetAll()
        {
            // Act 
            var page = Get<Page<Event>>("/api/events");

            // Assert
            Assert.NotEmpty(page.Content);
        }

        [Fact]
        public void Search_PartialQuery_ReturnEvent()
        {
            // Act 
            var page = Get<Page<Event>>("/api/events?q=Eric");

            // Assert
            Assert.Equal("Eric Clapton", page.Content.First().Name);
        }

        [Fact]
        public void Search_PartialQueryLowercase_ReturnEvent()
        {
            // Act 
            var page = Get<Page<Event>>("/api/events?q=eri");

            // Assert
            Assert.Equal("Eric Clapton", page.Content.First().Name);
        }

        [Fact]
        public void Search_WithinDates_ReturnEvents()
        {
            // Act 
            var page = Get<Page<Event>>("/api/events?since=01/01/2016&until=01/01/2017");

            // Assert
            Assert.NotEmpty(page.Content);
        }
    }
}
