﻿using EpamLabs.TicketResale.Presentation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;

namespace EpamLabs.TicketResale.Tests.Integration
{
    public class BaseTest
    {
        protected readonly TestServer _server;
        protected readonly HttpClient _client;

        public BaseTest()
        {
            var path = Directory.GetParent(Directory.GetCurrentDirectory()) +
                       "\\EpamLabs.TicketResale.Presentation";

            // Arrange
            _server = new TestServer(
            new WebHostBuilder()
                .UseContentRoot(path)
                .UseStartup<Startup>()
            );
            _client = _server.CreateClient();
        }

        protected TResponse Get<TResponse>(string querystring = "")
        {
            var response = _client.GetAsync(querystring).Result;
            response.EnsureSuccessStatusCode();
            var content = response.Content.ReadAsStringAsync().Result;
            var page = JsonConvert.DeserializeObject<TResponse>(content);
            return page;
        }
    }
}
