﻿using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace EpamLabs.TicketResale.Tests.Integration
{
    public class VenuesApiTest : BaseTest
    {
        [Fact]
        public void GetAll()
        {
            // Act 
            var page = Get<Page<Venue>>("/api/venues");

            // Assert
            Assert.NotEmpty(page.Content);
        }

        [Fact]
        public void Search_PartialQuery_ReturnVenue()
        {
            // Act 
            var page = Get<Page<Venue>>("/api/venues?q=Bar");

            // Assert
            Assert.Equal("Bar Kvartirnik", page.Content.First().Name);
        }

        [Fact]
        public void Search_PartialQueryLowercase_ReturnVenue()
        {
            // Act 
            var page = Get<Page<Venue>>("/api/venues?q=bar");

            // Assert
            Assert.Equal("Bar Kvartirnik", page.Content.First().Name);
        }
    }
}
