﻿using EpamLabs.TicketResale.Data;
using EpamLabs.TicketResale.Presentation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net.Http;
using Xunit;

namespace EpamLabs.TicketResale.Tests.Integration
{
    public class CitiesApiTest : BaseTest
    {
        [Fact]
        public void GetAll()
        {
            // Act 
            var page = Get<Page<City>>("/api/cities");

            // Assert
            Assert.NotEmpty(page.Content);
        }

        [Fact]
        public void Search_PartialQuery_ReturnCity()
        {
            // Act 
            var page = Get<Page<City>>("/api/cities?q=Gom");

            // Assert
            Assert.Equal("Gomel", page.Content.First().Name);
        }

        [Fact]
        public void Search_PartialQueryLowercase_ReturnCity()
        {
            // Act 
            var page = Get<Page<City>>("/api/cities?q=gom");

            // Assert
            Assert.Equal("Gomel", page.Content.First().Name);
        }
    }
}
