﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Infrastructure
{
    public class UnauthorizedException : ServiceException
    {
        public UnauthorizedException(string message) : base(message) { }
    }
}
