﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Infrastructure
{
    public class NotFoundException : ServiceException
    {
        public NotFoundException(string message) : base(message) { }
    }
}
