﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public static class IdentityExtensions
    {
        public static IdentityBuilder AddCustomUserStore(this IdentityBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }
            var services = builder.Services;

            services.AddSingleton<IUserStore<ApplicationUser>, CustomUserStore>();
            services.AddSingleton<IUserPasswordStore<ApplicationUser>, CustomUserStore>();
            services.AddSingleton<IUserRoleStore<ApplicationUser>, CustomUserStore>();
            services.AddSingleton<IRoleStore<ApplicationRole>, CustomRoleStore>();

            return builder;
        }

    }
}
