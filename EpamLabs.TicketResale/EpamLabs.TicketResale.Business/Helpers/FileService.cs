﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public class FileService
    {
        // If successful returns path of the uploaded file relative to the basePath
        public string UploadFile(IFormFile file, string basePath, string subPath, string fileName)
        {
            if (file != null)
            {
                var uploadTo = Path.Combine(basePath, subPath);
                if (file.Length > 0)
                {
                    var filePath = Path.Combine(uploadTo, fileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    return $"/{subPath}/{fileName}";
                }
            }
            return null;
        }

        public void RemoveFile(string basePath, string subPath)
        {
            if (subPath != null && basePath != null)
            {
                var sub = subPath.Replace('/', Path.DirectorySeparatorChar);
                var fp = basePath + sub;
                var fullPath = Path.Combine(basePath, sub);
                File.Delete(fp);
            }
        }

        public string StampFileNameWithDateTime(string fileName)
        {
            var fileInfo = new FileInfo(fileName);
            var name = fileInfo.Name.Replace(fileInfo.Extension, string.Empty);
            var newName = name + DateTime.Now.ToString("_yyyy_dd_MM_HH_mm_ss");
            return newName + fileInfo.Extension;
        }
    }
}
