﻿using EpamLabs.TicketResale.Data;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Search
{
    public static class ElasticSearchExtensions
    {
        public static IServiceCollection AddElasticSearch(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<ISearchService, SearchService>();
            using (var serviceScope = serviceCollection.BuildServiceProvider().GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var events = serviceScope.ServiceProvider.GetService<IEventService>();
                var venues = serviceScope.ServiceProvider.GetService<IVenueService>();
                var cities = serviceScope.ServiceProvider.GetService<ICityService>();
                serviceScope.ServiceProvider.GetService<ISearchService>().UpdateDefaultIndex(events, venues, cities);
            }

            return serviceCollection;
        }

        public static void UpdateDefaultIndex(this ISearchService searchService,
            IEventService events,
            IVenueService venues, 
            ICityService cities)
        {
            searchService.DeleteDefaultIndex();
            searchService.CreateIndex();
            searchService.AddMany(events.GetAll());
            searchService.AddMany(venues.GetAll());
            searchService.AddMany(cities.GetAll());
        }
    }
}
