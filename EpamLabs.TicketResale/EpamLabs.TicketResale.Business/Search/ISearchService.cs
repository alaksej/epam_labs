﻿using EpamLabs.TicketResale.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Search
{
    public interface ISearchService
    {
        bool IsAvailable { get; }

        ISearchResponse<T> SuggestNameCompletions<T>(string query, int totalElements) where T : class;
        ISearchResponse<T> SimpleQuerySearchByName<T>(string query, int totalElements) where T : class;
        ISearchResponse<T> Complete<T>(string query, int totalElements) where T : class;
        void AddOrUpdate<T>(T entity) where T : class;
        void AddMany<T>(IEnumerable<T> entities) where T : class;
        void CreateIndex();
        void Delete<T>(object id) where T : class;
        void DeleteDefaultIndex();
    }
}
