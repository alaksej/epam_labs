﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Search
{
    public class ElasticsearchOptions
    {
        public string Uri { get; set; }
        public string DefaultIndex { get; set; }
    }
}
