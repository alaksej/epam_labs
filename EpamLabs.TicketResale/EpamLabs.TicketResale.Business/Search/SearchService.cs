﻿using EpamLabs.TicketResale.Data;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamLabs.TicketResale.Business.Search
{
    public class SearchService : ISearchService
    {
        private readonly IElasticClient _client;
        private readonly IConnectionSettingsValues _settings;
        private readonly string _defaultIndex;
        private readonly ILogger<ISearchService> _logger;

        public SearchService(IOptions<ElasticsearchOptions> optionsAccessor, ILogger<ISearchService> logger)
        {
            _logger = logger;
            var options = optionsAccessor.Value;
            var node = new Uri(options.Uri);
            _defaultIndex = options.DefaultIndex;

            _settings = new ConnectionSettings(node)
                .DefaultIndex(options.DefaultIndex);
                //.MapPropertiesFor<Event>(d => d
                //    .Ignore(e => e.Date)
                //    .Ignore(e => e.Description)
                //    .Ignore(e => e.Image)
                //    .Ignore(e => e.Tickets)
                //    .Ignore(e => e.Url)
                //    .Ignore(e => e.Venue)
                //    .Ignore(e => e.VenueId))
                //.MapPropertiesFor<Venue>(d => d
                //    .Ignore(v => v.Address)
                //    .Ignore(v => v.City)
                //    .Ignore(v => v.CityId)
                //    .Ignore(v => v.Events))
                //.MapPropertiesFor<City>(d => d
                //    .Ignore(c => c.Venues));

            _client = new ElasticClient(_settings);
        }

        public bool IsAvailable => _client.ClusterHealth(x => x.Index(_defaultIndex)).IsValid;

        public void DeleteDefaultIndex()
        {
            if (_client.IndexExists(_defaultIndex).Exists)
                _logger.LogInformation(_client.DeleteIndex(_defaultIndex).DebugInformation);
        }

        public void CreateIndex()
        {
            var response = _client.CreateIndex(_defaultIndex, i => i
                .Settings(s => s
                    .NumberOfReplicas(0)
                    .NumberOfShards(2)
                    .Analysis(a => a
                        .Tokenizers(t => t
                            .NGram("my_ngram", n => n
                                .MaxGram(4)
                                .MinGram(2)
                            )
                            .Pattern("my_pattern", p => p
                                .Pattern(@"\W+")
                            )
                        )
                        .TokenFilters(tf => tf
                            .NGram("ngram_filter", n => n
                                .MaxGram(4)
                                .MinGram(2)
                            )
                            .WordDelimiter("my_words", w => w
                                .SplitOnCaseChange()
                                .PreserveOriginal()
                                .SplitOnNumerics()
                                .GenerateNumberParts()
                                .GenerateWordParts()
                            )
                        )
                        .Analyzers(an => an
                            .Custom("my_ngram_analyzer", ca => ca
                                .Tokenizer("standard")
                                .Filters("ngram_filter", "lowercase")
                            )
                            .Custom("my_keyword", c => c
                                .Tokenizer("keyword")
                                .Filters("lowercase")
                            )
                        )
                    )
                )
                .Mappings(m => m
                    .Map<City>(map => map
                        .AutoMap()
                        .Properties(ps => ps
                            .Nested<Venue>(n => n
                                .Name(p => p.Venues.First())
                                .AutoMap()
                            )
                        )
                    )
                    .Map<Venue>(map => map
                        .AutoMap()
                        .Properties(ps => ps
                            .Nested<Event>(n => n
                                .Name(v => v.Events.First())
                                .AutoMap()
                                .Properties(pps => pps
                                    .Text(t => t
                                        .Name(p => p.Name)
                                        .Analyzer("my_ngram_analyzer")

                                    )
                                )
                            )
                        )
                    )
                )
            );
            _logger.LogInformation(response.DebugInformation);
        }

        public void AddMany<T>(IEnumerable<T> entities) where T : class
        {
            _client.IndexMany(entities);
        }

        public void AddOrUpdate<T>(T entity) where T : class
        {
            var response = _client.Index(entity, x => x.Refresh(Elasticsearch.Net.Refresh.True));
        }

        public void Delete<T>(object id) where T : class
        {
            var response = _client.Delete(new DocumentPath<T>(new Id(id.ToString())));
        }

        public ISearchResponse<T> SuggestNameCompletions<T>(string query, int totalElements) where T : class
        {
            var response = _client.Search<T>(d => d
                    .Size(totalElements)
                    .Suggest(x => x
                        .Completion("autocomplete", y => y
                            .Field("name")
                            .Prefix(query)
                        )
                    )
                );
            _logger.LogInformation(response.DebugInformation);
            return response;
        }

        public ISearchResponse<T> Complete<T>(string query, int totalElements) where T : class
        {
            var response = _client.Search<T>(d => d
                .Size(totalElements)
            //.Suggest(s => s
            //    .Completion("autocomplete", c => c
            //        .Field("name")
            //        .Prefix(query)
            //    )
            //)
                .Query(q => q
                    .Wildcard(m => m
                        .Field("name")
                        .Value($"{query}*")
                     )
                )
            );
            _logger.LogInformation(response.DebugInformation);
            return response;
        }

        public ISearchResponse<T> SimpleQuerySearchByName<T>(string query, int totalElements) where T : class
        {
            var response = _client.Search<T>(s => s
                .Size(totalElements)
                .Query(q => q
                    .MultiMatch(m => m
                        .Fields("name")
                        .Query(query)
                        .FuzzyRewrite(RewriteMultiTerm.ConstantScoreBoolean)
                        .Fuzziness(Fuzziness.Auto)
                    )
                )
            );
            _logger.LogInformation(response.DebugInformation);
            return response;
        }
    }
}
