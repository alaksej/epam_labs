﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public class ResourcesCache : IResourcesCache
    {
        private readonly ConcurrentDictionary<string, string> _cache = new ConcurrentDictionary<string, string>();

        public string AddOrUpdate(string cacheKey, Func<string, string> valueFactory, Func<string, string, string> updateFactory)
        {
            return _cache.AddOrUpdate(cacheKey, valueFactory, updateFactory);
        }
    }
}
