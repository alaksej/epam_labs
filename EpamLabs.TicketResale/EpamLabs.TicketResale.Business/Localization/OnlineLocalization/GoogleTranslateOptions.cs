﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public class GoogleTranslateOptions
    {
        public string ApiKey { get; set; }
        public string ApplicationName { get; set; }
    }
}
