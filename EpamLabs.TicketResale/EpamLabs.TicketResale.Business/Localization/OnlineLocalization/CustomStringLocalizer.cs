﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Globalization;


namespace EpamLabs.TicketResale.Business.Localization
{
    public class CustomStringLocalizer : IStringLocalizer
    {
        private readonly IResourceManager _resourceManager;
        private readonly CultureInfo _defaultCulture;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CustomStringLocalizer(IResourceManager resourceManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _resourceManager = resourceManager;
            _httpContextAccessor = httpContextAccessor;
            var httpContext = httpContextAccessor.HttpContext;
            var options = httpContext.RequestServices.GetRequiredService<IOptions<RequestLocalizationOptions>>().Value;
            _defaultCulture = options.DefaultRequestCulture.Culture;
        }

        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            CultureInfo.DefaultThreadCurrentCulture = culture;
            return new CustomStringLocalizer(_resourceManager, _httpContextAccessor);
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeAncestorCultures)
        {
            return new List<LocalizedString>();
        }

        private string GetString(string name)
        {
            var culture = CultureInfo.CurrentCulture.Name;
            var defaultCulture = _defaultCulture.Name;
            if (culture == null || culture == defaultCulture)
            {
                return name;
            }
            return _resourceManager.GetString(name, culture);
        }
    }
}
