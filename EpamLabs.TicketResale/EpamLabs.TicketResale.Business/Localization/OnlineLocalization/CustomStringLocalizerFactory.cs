﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Http;

namespace EpamLabs.TicketResale.Business.Localization
{
    public class CustomStringLocalizerFactory : IStringLocalizerFactory
    {
        private readonly IResourceManager _resourceManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CustomStringLocalizerFactory(
            IResourceManager resourceManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _resourceManager = resourceManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            return new CustomStringLocalizer(_resourceManager, _httpContextAccessor);
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            return new CustomStringLocalizer(_resourceManager, _httpContextAccessor);
        }
    }
}
