﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public interface IResourcesCache
    {
        string AddOrUpdate(string cacheKey, Func<string, string> valueFactory, Func<string, string, string> updateFactory);
    }
}
