﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public interface IResourceManager
    {
        string GetString(string name, string culture);
    }
}
