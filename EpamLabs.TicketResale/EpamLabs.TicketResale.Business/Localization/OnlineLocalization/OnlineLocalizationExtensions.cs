﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;

namespace EpamLabs.TicketResale.Business.Localization
{
    public static class OnlineLocalizationExtensions
    {
        public static IMvcBuilder AddOnlineLocalization(this IMvcBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }
            var services = builder.Services;

            services.AddSingleton<IStringLocalizerFactory, CustomStringLocalizerFactory>();
            services.AddSingleton<IResourcesCache, ResourcesCache>();
            services.AddScoped<IResourceManager, OnlineResourceManager>();
            services.AddScoped<ITranslateService, GoogleTranslateService>();

            return builder;
        }
    }
}
