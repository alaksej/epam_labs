﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public class OnlineResourceManager : IResourceManager
    {
        private readonly IResourcesCache _cache;
        private readonly ITranslateService _translateService;
        private readonly ILogger<OnlineResourceManager> _logger;

        public OnlineResourceManager(
            IResourcesCache cache,
            ITranslateService translateService, 
            ILoggerFactory loggerFactory)
        {
            _cache = cache;
            _translateService = translateService;
            _logger = loggerFactory.CreateLogger<OnlineResourceManager>();
        }

        public string GetString(string name, string culture)
        {
            var cacheKey = GetCacheKey(name, culture);

            var value = _cache.AddOrUpdate(
                cacheKey, 
                k =>
                    {
                        return _translateService.Translate(name, culture);
                    }, 
                (k, v) => 
                    {
                        if (v == null)
                        {
                            return _translateService.Translate(name, culture);
                        }
                        return v;
                    });
            return value;
        }


        private string GetCacheKey(string name, string culture)
        {
            return $"name={name}&culture={culture ?? (CultureInfo.CurrentUICulture).Name}";
        }
    }
}
