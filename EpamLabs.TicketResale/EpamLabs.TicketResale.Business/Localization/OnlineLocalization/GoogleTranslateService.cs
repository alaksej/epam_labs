﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;

using Google.Apis.Services;
using Google.Apis.Translate.v2;
using TranslationsListResponse = Google.Apis.Translate.v2.Data.TranslationsListResponse;
using Google;
using Microsoft.Extensions.Options;

namespace EpamLabs.TicketResale.Business.Localization
{
    public class GoogleTranslateService : ITranslateService
    {
        private readonly TranslateService _service;
        private readonly ILogger<GoogleTranslateService> _logger;

        public GoogleTranslateService(
            IOptions<GoogleTranslateOptions> optionsAccessor,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<GoogleTranslateService>();
            var options = optionsAccessor.Value;
            _service = new TranslateService(new BaseClientService.Initializer()
            {
                ApiKey = options.ApiKey,
                ApplicationName = options.ApplicationName
            });
        }

        public string Translate(string sourceText, string toLanguage)
        {
            if (sourceText == null)
            {
                throw new ArgumentNullException(nameof(sourceText));
            }

            string[] srcText = new[] { sourceText };
            TranslationsListResponse response = null;
            try
            {
                response = _service.Translations.List(srcText, toLanguage).ExecuteAsync().Result;
            }
            catch (AggregateException ae)
            {
                _logger.LogWarning(new EventId(), ae, $"Exception thrown while trying to translate {sourceText} to language: {toLanguage}");
                ae.Handle(x =>
                {
                    if (x is HttpRequestException || x is GoogleApiException)
                    {
                        _logger.LogInformation("Connection problem while accessing GoogleTranslate");
                        return true;
                    }
                    return false;
                });
            }
            if (response != null && response.Translations.Count > 0)
            {
                var translation = response.Translations[0];
                var translatedText = translation.TranslatedText;
                translatedText = MatchFirstLetterCasing(sourceText, translatedText);
                _logger.LogInformation($"Translated '{sourceText}' to '{translatedText}'. Detected language: {translation.DetectedSourceLanguage}");
                return translatedText;
            }
            _logger.LogInformation($"Unable to translate {sourceText} to language: {toLanguage}");
            return null;
        }

        private string MatchFirstLetterCasing(string template, string toFix)
        {
            if (StartsUpper(template))
            {
                return UppercaseFirst(toFix);
            }
            else
            {
                return LowercaseFirst(toFix);
            }
        }

        private bool StartsUpper(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return false;
            }
            var a = s.ToCharArray();
            return char.IsUpper(a[0]);
        }

        private string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        private string LowercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToLower(a[0]);
            return new string(a);
        }
    }
}
