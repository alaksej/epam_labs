﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Localization
{
    public interface ITranslateService
    {
        string Translate(string sourceText, string toLanguage);
    }
}
