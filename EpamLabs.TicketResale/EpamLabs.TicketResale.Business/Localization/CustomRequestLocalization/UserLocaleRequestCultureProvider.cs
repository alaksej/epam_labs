﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Internal;
using EpamLabs.TicketResale.Business;
using EpamLabs.TicketResale.Data;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace EpamLabs.TicketResale.Business
{
    public class UserLocaleRequestCultureProvider : RequestCultureProvider
    {
        public override Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }
            var userService = httpContext.RequestServices.GetRequiredService<IUserService>();
            var user = userService.GetCurrent();
            var userCulture = user?.Locale;
            if (userCulture == null)
            {
                return TaskCache<ProviderCultureResult>.DefaultCompletedTask;
            }
            var providerResultCulture = new ProviderCultureResult(userCulture);
            return Task.FromResult(providerResultCulture);
        }
    }
}
