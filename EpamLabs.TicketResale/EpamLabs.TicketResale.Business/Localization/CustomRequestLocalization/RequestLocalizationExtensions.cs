﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using System.Linq;
using Microsoft.AspNetCore.Localization.Routing;
using System.Globalization;
using Microsoft.Extensions.DependencyInjection;

namespace EpamLabs.TicketResale.Business
{
    public static class RequestLocalizationExtensions
    {
        public static IApplicationBuilder UseCustomRequestLocalization(this IApplicationBuilder app, 
            string[] supportedCultureNames,
            string routeDataStringKey = "culture",
            int defaultCultureIndex = 0)
        {
            var supportedCultures = supportedCultureNames.Select(x => new CultureInfo(x)).ToList();
            var defaultCulture = supportedCultureNames[defaultCultureIndex];

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value;
            options.DefaultRequestCulture = new RequestCulture(culture: defaultCulture, uiCulture: defaultCulture);
            options.SupportedCultures = supportedCultures;
            options.SupportedUICultures = supportedCultures;
            options.RequestCultureProviders.Insert(0, new RouteDataRequestCultureProvider()
            {
                Options = options,
                RouteDataStringKey = routeDataStringKey
            });
            options.RequestCultureProviders.Insert(1, new UserLocaleRequestCultureProvider());
            app.UseRequestLocalization(options);
            return app;
        }
    }
}
