﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public interface ILocalizationService
    {
        void SetCultureCookie(string culture);
        //void SetCultureCookieFromCurrentUser();
        string AdaptUrlToCulture(string localUrl, string culture);
    }
}
