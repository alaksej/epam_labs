﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;

namespace EpamLabs.TicketResale.Business
{
    public class LocalizationService : ILocalizationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<LocalizationService> _logger;

        public LocalizationService(
            IHttpContextAccessor httpContextAccessor,
            ILoggerFactory loggerFactory)
        {
            _httpContextAccessor = httpContextAccessor;
            _logger = loggerFactory.CreateLogger<LocalizationService>();
        }

        public void SetCultureCookie(string culture)
        {
            _httpContextAccessor.HttpContext.Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            _logger.LogInformation("Culture cookie added");
        }

        public string AdaptUrlToCulture(string localUrl, string culture)
        {
            if (localUrl != null)
            {
                var httpContext = _httpContextAccessor.HttpContext;
                var options = httpContext.RequestServices.GetRequiredService<IOptions<RequestLocalizationOptions>>().Value;
                var supportedCultures = options.SupportedCultures.Select(x => x.Name).ToList();
                var routeData = localUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (routeData.Length > 0)
                {
                    var routeCulture = routeData[0];
                    if (supportedCultures.Contains(routeCulture))
                    {
                        routeData[0] = culture;
                        var newRoute = "/" + String.Join("/", routeData);
                        return newRoute;
                    }
                }
            }
            return "/" + culture;
        }
    }
}
