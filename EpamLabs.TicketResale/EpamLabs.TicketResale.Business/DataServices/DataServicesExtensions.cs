﻿using Microsoft.Extensions.DependencyInjection;

namespace EpamLabs.TicketResale.Business
{
    public static class DataServicesExtensions
    {
        public static IServiceCollection AddDataServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IOrderService, OrderService>();
            serviceCollection.AddScoped<IEventService, EventService>();
            serviceCollection.AddScoped<ITicketService, TicketService>();
            serviceCollection.AddScoped<ICityService, CityService>();
            serviceCollection.AddScoped<IVenueService, VenueService>();
            serviceCollection.AddScoped<IUserService, UserService>();
            serviceCollection.AddScoped<FileService>();

            return serviceCollection;
        }
    }
}
