﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public interface IUserService : IEntityService<ApplicationUser, string>
    {
        ApplicationUser GetCurrent();
        void UpdateUserCulture(string newCulture);
        ApplicationUser GetByName(string userName);
        void UpdateUserInfo(ApplicationUser model);
    }
}
