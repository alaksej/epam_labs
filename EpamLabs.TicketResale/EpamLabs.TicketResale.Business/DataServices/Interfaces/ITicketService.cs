﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public interface ITicketService : IEntityService<Ticket, int>
    {
        IEnumerable<Ticket> GetForSale(Event e);
        IEnumerable<Ticket> GetForSale(Event e, ApplicationUser exceptUser);
        IEnumerable<Ticket> GetForUser(ApplicationUser user);
        IEnumerable<Ticket> GetForUser(ApplicationUser user, TicketState state);
    }
}
