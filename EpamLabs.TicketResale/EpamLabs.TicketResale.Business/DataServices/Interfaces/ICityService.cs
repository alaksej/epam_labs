﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EpamLabs.TicketResale.Business
{
    public interface ICityService : IEntityService<City, int>
    {
        IEnumerable<SelectListItem> GetAllAsSelectList();
        Page<City> Search(string query, string sortBy, int? pageNumber, int? size);
    }
}
