﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EpamLabs.TicketResale.Business
{
    public interface IVenueService : IEntityService<Venue, int>
    {
        IEnumerable<SelectListItem> GetAllAsSelectList();
        Page<Venue> Search(string query, string sortBy, int? page, int? size);
    }
}
