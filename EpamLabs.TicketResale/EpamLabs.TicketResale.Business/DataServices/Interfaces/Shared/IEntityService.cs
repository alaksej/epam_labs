﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;

namespace EpamLabs.TicketResale.Business
{
    public interface IEntityService<TEntity, TKey> 
                                where TEntity : class, IEntity<TKey>
                                where TKey : IEquatable<TKey>
    {
        TEntity GetById(TKey id);
        IEnumerable<TEntity> GetAll();
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        bool IsSafeToDelete(TKey id);
    }
}
