﻿using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public interface IOrderService : IEntityService<Order, int>
    {
        IEnumerable<Order> GetForBuyer(ApplicationUser buyer);
        IEnumerable<Order> GetForSeller(ApplicationUser seller, OrderState state);
        void PlaceOrder(ApplicationUser buyer, int ticketId);
        void CancelOrder(ApplicationUser buyer, int orderId);
        void ConfirmOrder(ApplicationUser seller, Order order);
        void RejectOrder(ApplicationUser seller, Order order);
    }
}
