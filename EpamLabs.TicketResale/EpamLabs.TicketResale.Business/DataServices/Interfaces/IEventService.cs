﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace EpamLabs.TicketResale.Business
{
    public interface IEventService : IEntityService<Event, int>
    {
        Page<Event> GetUpcoming(int page, int size);
        string UploadImage(IFormFile file, string basePath, string subPath);
        Event UpdateImage(int eventId, IFormFile image, string basePath, string subPath);
        IEnumerable<SelectListItem> GetAllAsSelectList();
        Page<Event> Query(string query, DateTime since, DateTime until, int[] venueIds, int[] cityIds, string sortBy, int? page, int? size);
        IEnumerable<Event> SuggestCompletions(string query, int size = 10);
    }
}
