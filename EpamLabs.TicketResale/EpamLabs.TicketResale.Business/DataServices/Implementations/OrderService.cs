﻿using EpamLabs.TicketResale.Business.Infrastructure;
using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public class OrderService : EntityService<Order, int>, IOrderService
    {
        private IGenericRepository<Order> _orders;
        private IGenericRepository<Event> _events;
        private IGenericRepository<Ticket> _tickets;
        private IGenericRepository<Venue> _venues;
        private IGenericRepository<City> _cities;
        private IGenericRepository<ApplicationUser> _users;

        public OrderService(
            IGenericRepository<Order> orders,
            IGenericRepository<Event> events,
            IGenericRepository<Ticket> tickets,
            IGenericRepository<Venue> venues,
            IGenericRepository<City> cities,
            IGenericRepository<ApplicationUser> users,
            ISearchService searchService) : base(orders, searchService)
        {
            _orders = orders;
            _events = events;
            _tickets = tickets;
            _users = users;
            _venues = venues;
            _cities = cities;
        }

        public override IEnumerable<Order> GetAll()
        {
            var all = base.GetAll();
            return LoadNavigationProperties(all);
        }

        public override Order GetById(int id)
        {
            return LoadNavigationProperties(base.GetById(id));
        }

        public IEnumerable<Order> GetForBuyer(ApplicationUser buyer)
        {
            var orders = _orders.Find(x => x.UserId == buyer.Id);
            return LoadNavigationProperties(orders);
        }

        public IEnumerable<Order> GetForSeller(ApplicationUser seller, OrderState state)
        {
            var orders = _orders.Find(x => x.State == state);
            return LoadNavigationProperties(orders).Where(x => x.Ticket.UserId == seller.Id);
        }

        public void PlaceOrder(ApplicationUser user, int ticketId)
        {
            if (user == null)
            {
                throw new UnauthorizedException("User is null");
            }
            var ticket = _tickets.GetById(ticketId);
            if (ticket == null)
            {
                throw new NotFoundException($"Ticket with id {ticketId} not found.");
            }
            if (ticket.UserId == user.Id)
            {
                throw new UnauthorizedException($"Placing orders to buy your own tickets is not allowed.");
            }
            ticket.State = TicketState.Ordered;
            ticket = _tickets.Update(ticket);
            if (ticket == null)
            {
                throw new ServiceException($"Failed to update ticket. Ticket id {ticket.Id}");
            }
            var order = _orders.Add(new Order
            {
                State = OrderState.Pending,
                TicketId = ticketId,
                UserId = user.Id
            });
            if (order == null)
            {
                throw new ServiceException($"Failed to add new order.");
            }
        }

        public void CancelOrder(ApplicationUser buyer, int orderId)
        {
            if (buyer == null)
            {
                throw new UnauthorizedException("buyer is null.");
            }
            var order = _orders.GetById(orderId);
            if (order == null)
            {
                throw new NotFoundException("Order with id {orderId} not found.");
            }
            if (order.User.Id != buyer.Id)
            {
                throw new UnauthorizedException("Unauthorized attempt to cancel order of another user.");
            }
            var ticket = _tickets.GetById(order.TicketId);
            if (ticket == null)
            {
                throw new ServiceException($"Ticket with id {order.TicketId} not found.");
            }
            if (_orders.Delete(order) != null)
            {
                ticket.State = TicketState.ForSale;
                ticket = _tickets.Update(ticket);
                if (ticket == null)
                {
                    throw new ServiceException("Failed to update a ticket.");
                }
            }
            else
            {
                throw new ServiceException("Failed to delete an order.");
            }

        }

        public void ConfirmOrder(ApplicationUser seller, Order order)
        {
            if (seller == null)
            {
                throw new UnauthorizedException("seller is null.");
            }

            order.State = OrderState.Confirmed;
            var ticket = _tickets.GetById(order.TicketId);
            if (ticket == null)
            {
                throw new NotFoundException($"Ticket with id {order.TicketId} not found.");
            }

            ticket.State = TicketState.Sold;
            ticket = _tickets.Update(ticket);
            if (ticket == null)
            {
                throw new ServiceException("Failed to update a ticket.");
            }

            order = _orders.Update(order);
            if (order == null)
            {
                throw new ServiceException("Failed to update an order.");
            }
        }

        public void RejectOrder(ApplicationUser seller, Order order)
        {
            if (seller == null)
            {
                throw new UnauthorizedException("seller is null.");
            }
            order.State = OrderState.Rejected;
            var ticket = _tickets.GetById(order.TicketId);
            if (ticket == null)
            {
                throw new NotFoundException($"Ticket with id {order.TicketId} not found.");
            }
            ticket.State = TicketState.ForSale;
            ticket = _tickets.Update(ticket);
            if (ticket == null)
            {
                throw new ServiceException("Failed to update a ticket.");
            }

            if (ticket == null)
            {
                throw new ServiceException("Failed to update a ticket.");
            }

            order = _orders.Update(order);
            if (order == null)
            {
                throw new ServiceException("Failed to update an order.");
            }
        }

        private IEnumerable<Order> LoadNavigationProperties(IEnumerable<Order> orders)
        {
            if (orders != null)
            {
                foreach (var item in orders)
                {
                    LoadNavigationProperties(item);
                }
            }
            return orders;
        }

        private Order LoadNavigationProperties(Order item)
        {
            if (item != null)
            {
                item.Ticket = _tickets.Find(x => x.Id == item.TicketId).FirstOrDefault();
                item.Ticket.Event = _events.Find(x => x.Id == item.Ticket.EventId).FirstOrDefault();
                item.Ticket.Event.Venue = _venues.Find(x => x.Id == item.Ticket.Event.VenueId).FirstOrDefault();
                item.Ticket.Event.Venue.City = _cities.Find(x => x.Id == item.Ticket.Event.Venue.CityId).FirstOrDefault();
                item.Ticket.User = _users.Find(x => x.Id == item.Ticket.UserId).FirstOrDefault();
                item.User = _users.Find(x => x.Id == item.UserId).FirstOrDefault();
            }
            return item;
        }
    }
}
