﻿using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public class TicketService : EntityService<Ticket, int>, ITicketService
    {
        private IGenericRepository<Order> _orders;
        private IGenericRepository<Event> _events;
        private IGenericRepository<Ticket> _tickets;
        private IGenericRepository<Venue> _venues;
        private IGenericRepository<City> _cities;
        private IGenericRepository<ApplicationUser> _users;

        public TicketService(
            IGenericRepository<Order> orders,
            IGenericRepository<Event> events,
            IGenericRepository<Ticket> tickets,
            IGenericRepository<Venue> venues,
            IGenericRepository<City> cities,
            IGenericRepository<ApplicationUser> users,
            ISearchService searchService) : base(tickets, searchService)
        {
            _orders = orders;
            _events = events;
            _tickets = tickets;
            _users = users;
            _venues = venues;
            _cities = cities;
        }

        public override Ticket GetById(int id)
        {
            return LoadNavigationProperties(base.GetById(id));
        }

        public override IEnumerable<Ticket> GetAll()
        {
            var all = base.GetAll();
            return LoadNavigationProperties(all);
        }

        public IEnumerable<Ticket> GetForSale(Event e) 
        {
            var tickets = _tickets.Find(x => x.EventId == e.Id && x.State == TicketState.ForSale);
            return LoadNavigationProperties(tickets);
        }

        public IEnumerable<Ticket> GetForSale(Event e, ApplicationUser exceptUser)
        {
            if (exceptUser != null)
            {
                var tickets = _tickets.Find(x => x.EventId == e.Id
                    && x.State == TicketState.ForSale
                    && x.UserId != exceptUser.Id);
                return LoadNavigationProperties(tickets);
            }
            return GetForSale(e);
        }

        public IEnumerable<Ticket> GetForUser(ApplicationUser user)
        {
            var tickets = _tickets.Find(x => x.UserId == user.Id);
            return LoadNavigationProperties(tickets);
        }

        public IEnumerable<Ticket> GetForUser(ApplicationUser user, TicketState state)
        {
            var tickets = _tickets.Find(x => x.UserId == user.Id && x.State == state);
            return LoadNavigationProperties(tickets);
        }

        public override bool IsSafeToDelete(int id)
        {
            return (_orders.Find(x => x.TicketId == id).FirstOrDefault() == null);
        }

        private IEnumerable<Ticket> LoadNavigationProperties(IEnumerable<Ticket> tickets)
        {
            if (tickets != null)
            {
                foreach (var item in tickets)
                {
                    LoadNavigationProperties(item);
                }
            }
            return tickets;   
        }

        private Ticket LoadNavigationProperties(Ticket item)
        {
            if (item != null)
            {
                //item.Orders = _orders.Find(x => x.TicketId == item.Id).ToList();
                item.Event = _events.Find(x => x.Id == item.EventId).FirstOrDefault();
                item.Event.Venue = _venues.Find(x => x.Id == item.Event.VenueId).FirstOrDefault();
                item.Event.Venue.City = _cities.Find(x => x.Id == item.Event.Venue.CityId).FirstOrDefault();
                item.User = _users.Find(x => x.Id == item.UserId).FirstOrDefault();
            }
            return item;
        }

    }
}
