﻿using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamLabs.TicketResale.Business
{
    public class CityService : EntityService<City, int>, ICityService
    {
        private readonly IGenericRepository<City> _cities;
        private readonly IGenericRepository<Venue> _venues;

        public CityService(
            IGenericRepository<City> cities,
            IGenericRepository<Venue> venues,
            ISearchService searchService) : base(cities, searchService)
        {
            _cities = cities;
            _venues = venues;
        }

        public override IEnumerable<City> GetAll()
        {
            var cities =_cities.GetAll().ToList();
            if (cities != null)
            {
                foreach (var city in cities)
                {
                    city.Venues = _venues.GetQueryable().Where(v => v.CityId == city.Id).ToList();
                }
            }
            return cities;
        }

        public override bool IsSafeToDelete(int id)
        {
            return !_venues.GetQueryable().Any(x => x.CityId == id);
        }

        public IEnumerable<SelectListItem> GetAllAsSelectList()
        {
            var list = new List<SelectListItem>();
            list.AddRange(GetAll()
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));

            return list;
        }

        public Page<City> Search(string q, string sortBy, int? page, int? size)
        {
            var query = _cities.GetQueryable();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(e => e.Name.Contains(q));

            switch (sortBy)
            {
                case "desc":
                    query = query.OrderByDescending(e => e.Name);
                    break;
                case "asc":
                default:
                    query = query.OrderBy(e => e.Name);
                    break;
            }

            return new Page<City>(query, page, size);
        }
    }
}
