﻿using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamLabs.TicketResale.Business
{
    public class EventService : EntityService<Event, int>, IEventService
    {
        private const int TextSearchQuerySize = 1000;

        private readonly IGenericRepository<City> _cities;
        private readonly IGenericRepository<Event> _events;
        private readonly IGenericRepository<Ticket> _tickets;
        private readonly IGenericRepository<Venue> _venues;
        private readonly IGenericRepository<ApplicationUser> _users;
        private readonly ISearchService _searchService;
        private readonly FileService _fileService;

        public EventService(
            IGenericRepository<City> cities,
            IGenericRepository<Event> events,
            IGenericRepository<Ticket> tickets,
            IGenericRepository<Venue> venues,
            IGenericRepository<ApplicationUser> users,
            ISearchService searchService,
            FileService fileService) : base(events, searchService)
        {
            _cities = cities;
            _events = events;
            _tickets = tickets;
            _venues = venues;
            _users = users;
            _searchService = searchService;
            _fileService = fileService;
        }

        public override IEnumerable<Event> GetAll()
        {
            var all = base.GetAll().OrderBy(x => x.Date);
            return LoadNavigationProperties(all);
        }

        public override Event GetById(int id)
        {
            var e = base.GetById(id);
            if (e != null)
            {
                LoadNavigationProperties(e);
            }
            return e;
        }

        public Page<Event> GetUpcoming(int page, int size)
        {
            var p = new Page<Event>(_events.GetQueryable()
                .Where(e => e.Date >= DateTime.Now)
                .OrderBy(e => e.Date), page, size);
            LoadNavigationProperties(p.Content);
            return p;
        }

        public string UploadImage(IFormFile image, string basePath, string subPath)
        {
            if (image != null && image.Length > 0)
            {
                var uniqueFileName = _fileService.StampFileNameWithDateTime(image.FileName);
                return _fileService.UploadFile(image, basePath, subPath, uniqueFileName);
            }
            return null;
        }

        public Event UpdateImage(int eventId, IFormFile image, string basePath, string subPath)
        {
            var @event = GetById(eventId);
            if (@event != null)
            {
                _fileService.RemoveFile(basePath, @event.Image);
                @event.Image = UploadImage(image, basePath, subPath);
                Update(@event);
                return @event;
            }
            return null;
        }

        public override bool IsSafeToDelete(int id)
        {
            return !_tickets.GetAll().Any(x => x.EventId == id);
        }

        public IEnumerable<SelectListItem> GetAllAsSelectList()
        {
            var list = new List<SelectListItem>();
            list.AddRange(GetAll()
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Text = x.Name + ", " + x.Venue.Name + ", " + x.Venue.City.Name, Value = x.Id.ToString() }));

            return list;
        }

        public Page<Event> Query(
            string q = null, 
            DateTime since = default(DateTime), 
            DateTime until = default(DateTime), 
            int[] venueIds = null,  
            int[] cityIds = null, 
            string sortBy = "",
            int? pageNumber = 0,
            int? size = 5)
        {
            var query = SearchByName(q, TextSearchQuerySize);

            if (since != default(DateTime))
                query = query.Where(e => e.Date >= since);
            if (until != default(DateTime))
                query = query.Where(e => e.Date <= until);
            if (venueIds != null && venueIds.Length > 0)
                query = query.Where(e => venueIds.Any(venueId => venueId == e.VenueId));
            if (cityIds != null && cityIds.Length > 0)
                query = query.Where(e => cityIds.Any(cityId => cityId == e.Venue.CityId));

            switch (sortBy)
            {
                case "date_desc":
                    query = query.OrderByDescending(e => e.Date);
                    break;
                case "name_asc":
                    query = query.OrderBy(e => e.Name);
                    break;
                case "name_desc":
                    query = query.OrderByDescending(e => e.Name);
                    break;
                case "date_asc":
                    query = query.OrderBy(e => e.Date);
                    break;
            }
            var page = new Page<Event>(query, pageNumber, size);
            LoadNavigationProperties(page.Content);
            return page;
        }

        public IEnumerable<Event> SuggestCompletions(string q, int size)
        {
            var query = _events.GetQueryable();
            if (string.IsNullOrWhiteSpace(q))
            {
                return query;
            }

            if (_searchService.IsAvailable)
            {
                var ids = _searchService.Complete<Event>(q, size)
                                            .Hits.Select(x => x.Source.Id);
                return query.Where(e => ids.Any(id => e.Id == id));
            }
            else
            {
                return query.Where(e => e.Name.Contains(q)).Take(size);
            }
        }

        private IQueryable<Event> SearchByName(string q, int size)
        {
            var query = _events.GetQueryable();
            if (string.IsNullOrWhiteSpace(q))
            {
                return query;
            }

            if (_searchService.IsAvailable)
            {
                var ids = _searchService.SimpleQuerySearchByName<Event>(q, size)
                                            .Hits.Select(x => x.Source.Id);
                return query.Where(e => ids.Any(id => e.Id == id));
            }
            else 
            {
                return query.Where(e => e.Name.Contains(q)).Take(size);
            }
        }

        private Event LoadNavigationProperties(Event @event)
        {
            @event.Venue = _venues.Find(x => x.Id == @event.VenueId).FirstOrDefault();
            if (@event.Venue != null)
            {
                @event.Venue.City = _cities.Find(x => x.Id == @event.Venue.CityId).FirstOrDefault();
            }
            @event.Tickets = _tickets.Find(x => x.EventId == @event.Id).ToList();
            if (@event.Tickets != null)
            {
                foreach (var t in @event.Tickets)
                {
                    t.User = _users.Find(x => x.Id == t.UserId).FirstOrDefault();
                }
            }
            return @event;
        }

        private IEnumerable<Event> LoadNavigationProperties(IEnumerable<Event> events)
        {
            if (events != null)
            {
                foreach (var item in events)
                {
                    LoadNavigationProperties(item);
                }
            }
            
            return events;
        }
    }
}
