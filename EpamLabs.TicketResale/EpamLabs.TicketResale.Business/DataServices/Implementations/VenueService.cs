﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System;
using EpamLabs.TicketResale.Business.Search;

namespace EpamLabs.TicketResale.Business
{
    public class VenueService : EntityService<Venue, int>, IVenueService
    {
        private IGenericRepository<Venue> _venues;
        private IGenericRepository<Event> _events;
        private IGenericRepository<City> _cities;

        public VenueService(
            IGenericRepository<Venue> venues,
            IGenericRepository<Event> events,
            IGenericRepository<City> cities,
            ISearchService searchService) : base(venues, searchService)
        {
            _venues = venues;
            _events = events;
            _cities = cities;
        }

        public override IEnumerable<Venue> GetAll()
        {
            var all = base.GetAll();
            foreach (var item in all)
            {
                LoadCity(item);
                LoadEvents(item);
            }
            return all;
        }
        
        public IEnumerable<SelectListItem> GetAllAsSelectList()
        {
            var list = new List<SelectListItem>();
            list.AddRange(GetAll()
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Text = x.Name + ", " + x.City.Name, Value = x.Id.ToString() }));

            return list;
        }

        public override bool IsSafeToDelete(int id)
        {
            return !_events.GetAll().Any(x => x.VenueId == id);
        }

        public Page<Venue> Search(string q, string sortBy, int? page, int? size)
        {
            var query = _venues.GetQueryable();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(e => e.Name.Contains(q));

            switch (sortBy)
            {
                case "desc":
                    query = query.OrderByDescending(e => e.Name);
                    break;
                case "asc":
                default:
                    query = query.OrderBy(e => e.Name);
                    break;
            }

            var pageResult = new Page<Venue>(query, page, size);
            foreach (var item in pageResult.Content)
            {
                LoadCity(item);
            }

            return pageResult;
        }

        private void LoadCity(Venue item)
        {
            item.City = _cities.Find(x => x.Id == item.CityId).FirstOrDefault();
        }

        private void LoadEvents(Venue item)
        {
            item.Events = _events.Find(x => x.VenueId == item.Id).ToList();
        }

    }
}
