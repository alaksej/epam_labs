﻿using EpamLabs.TicketResale.Business.Infrastructure;
using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamLabs.TicketResale.Business
{
    public abstract class EntityService<TEntity, TKey> : IEntityService<TEntity, TKey>
                                                        where TEntity : class, IEntity<TKey> 
                                                        where TKey : IEquatable<TKey>
    {
        private IGenericRepository<TEntity> _repository;
        private readonly ISearchService _searchService;

        public EntityService(
            IGenericRepository<TEntity> repository,
            ISearchService searchService)
        {
            _repository = repository;
            _searchService = searchService;
        }

        public virtual IEnumerable<TEntity> GetAll() => _repository.GetAll().ToList();
        public virtual TEntity GetById(TKey id) => _repository.GetById(id);
        public virtual bool IsSafeToDelete(TKey id) => false;

        public virtual void Create(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            if (GetById(entity.Id) != null)
                throw new ServiceException($"Error adding entity of type {typeof(TEntity)}. Reason: the entity already exists.");
            var addedEntity = _repository.Add(entity);
            if (addedEntity == null)
                throw new ServiceException($"Failed to add an entity.");
            _searchService.AddOrUpdate(entity);
        }

        public virtual void Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            _repository.Update(entity);
            _searchService.AddOrUpdate(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            var retrieved = GetById(entity.Id);
            if (retrieved == null)
                throw new ServiceException($"Error deleting entity of type {typeof(TEntity)}. Reason: the entity was not found.");
            var deletedEntity = _repository.Delete(retrieved);
            if (deletedEntity == null)
                throw new Exception("Failed to delete an entity.");
            _searchService.Delete<TEntity>(entity.Id);
        }
    }
}
