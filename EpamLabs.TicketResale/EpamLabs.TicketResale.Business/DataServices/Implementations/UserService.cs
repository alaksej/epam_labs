﻿using EpamLabs.TicketResale.Business.Infrastructure;
using EpamLabs.TicketResale.Business.Search;
using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business
{
    public class UserService : EntityService<ApplicationUser, string>, IUserService
    {
        private IGenericRepository<Order> _orders;
        private IGenericRepository<Ticket> _tickets;
        private IGenericRepository<ApplicationUser> _users;
        private IGenericRepository<ApplicationRole> _roles;
        private IGenericRepository<IdentityUserRole<string>> _userRoles;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger _logger;

        public UserService(
            IGenericRepository<Order> orders,
            IGenericRepository<Ticket> tickets,
            IGenericRepository<ApplicationUser> users,
            IGenericRepository<ApplicationRole> roles,
            IGenericRepository<IdentityUserRole<string>> userRoles,
            ILoggerFactory loggerFactory,
            IHttpContextAccessor httpContextAccessor,
            ISearchService searchService) : base(users, searchService)
        {
            _orders = orders;
            _tickets = tickets;
            _users = users;
            _roles = roles;
            _userRoles = userRoles;
            _httpContextAccessor = httpContextAccessor;
            _logger = loggerFactory.CreateLogger<UserService>();
        }

        public override IEnumerable<ApplicationUser> GetAll()
        {
            var all = base.GetAll();
            foreach (var item in all)
            {
                item.Orders = _orders.Find(x => x.UserId == item.Id).ToList();
                item.Tickets = _tickets.Find(x => x.UserId == item.Id).ToList();
            }
            return all;
        }

        public ApplicationUser GetCurrent()
        {
            var userName = _httpContextAccessor.HttpContext.User.Identity.Name;

            return GetByName(userName);
        }

        public void UpdateUserCulture(string newCulture)
        {
            var user = GetCurrent();
            user.Locale = newCulture;
            Update(user);
            _logger.LogInformation($"Current user culture updated with the value: {newCulture}");
        }

        public ApplicationUser GetByName(string userName)
        {
            var user = _users.Find(x => x.UserName == userName).FirstOrDefault();

            return LoadNavigationProperties(user);
        }

        public void UpdateUserInfo(ApplicationUser model)
        {
            var user = GetCurrent();
            if (user.Id != model.Id)
            {
                throw new UnauthorizedAccessException();
            }
            CopyEditables(model, user);
            Update(user);
        }

        private void CopyEditables(ApplicationUser from, ApplicationUser to)
        {
            to.FirstName = from.FirstName;
            to.LastName = from.LastName;
            to.Email = from.Email;
            to.Address = from.Address;
        }

        private ApplicationUser LoadNavigationProperties(ApplicationUser user)
        {
            if (user != null)
            {
                var userRoles = _userRoles.Find(x => x.UserId == user.Id);
                if (userRoles != null)
                {
                    foreach (var userRole in userRoles)
                    {
                        user.Roles.Add(userRole);
                    }
                }
            }
            return user;
        }

    }
}
