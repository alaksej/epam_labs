﻿using EpamLabs.TicketResale.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EpamLabs.TicketResale.Business.Formatters
{
    public class CsvOutputFormatter : TextOutputFormatter
    {
        public CsvOutputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));

            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }

        protected override bool CanWriteType(Type type)
        {
            if (typeof(Page<Event>).IsAssignableFrom(type)
                || typeof(Page<Venue>).IsAssignableFrom(type)
                || typeof(Page<City>).IsAssignableFrom(type))
            {
                return base.CanWriteType(type);
            }
            return false;
        }

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            IServiceProvider serviceProvider = context.HttpContext.RequestServices;
            var logger = serviceProvider.GetService(typeof(ILogger<CsvOutputFormatter>)) as ILogger;

            var response = context.HttpContext.Response;

            var buffer = new StringBuilder();
            if (context.Object is Page<Event>)
            {
                var pageEvents = context.Object as Page<Event>;
                FormatPageEvents(buffer, pageEvents, logger);
            }
            else if (context.Object is Page<Venue>)
            {
                var pageVenues = context.Object as Page<Venue>;
                FormatPageVenues(buffer, pageVenues, logger);
            }
            else
            {
                var pageCities = context.Object as Page<City>;
                FormatPageCities(buffer, pageCities, logger);
            }
            return response.WriteAsync(buffer.ToString());
        }

        private void FormatPageCities(StringBuilder buffer, Page<City> pageCities, ILogger logger)
        {
            buffer.AppendLine("name,id");
            foreach (var item in pageCities.Content)
            {
                FormatCity(buffer, item);
                logger.LogInformation($"Writing {item.Name}");
            }
        }

        private void FormatCity(StringBuilder buffer, City c)
        {
            buffer.AppendLine($"\"{c.Name}\",{c.Id}");
        }

        private void FormatPageVenues(StringBuilder buffer, Page<Venue> pageVenues, ILogger logger)
        {
            buffer.AppendLine("name,address,city/name,city/id,id");
            foreach (var item in pageVenues.Content)
            {
                FormatVenue(buffer, item);
                logger.LogInformation($"Writing {item.Name}");
            }
        }

        private void FormatVenue(StringBuilder buffer, Venue v)
        {
            buffer.AppendLine($"\"{v.Name}\",\"{v.Address}\",\"{v.City?.Name}\",{v.City?.Id},{v.Id}");
        }

        private void FormatPageEvents(StringBuilder buffer, Page<Event> pageEvents, ILogger logger)
        {
            buffer.AppendLine("name,date,image,url,description,venue/name,venue/address,venue/city/name,venue/city/id,venue/id,id");
            foreach (var item in pageEvents.Content)
            {
                FormatEvent(buffer, item);
                logger.LogInformation($"Writing {item.Name}");
            }
        }

        private void FormatEvent(StringBuilder buffer, Event e)
        {
            buffer.AppendLine($"\"{e.Name}\",\"{e.Date}\",{e.Image},{e.Url},\"{e.Description}\",\"{e.Venue?.Name}\",\"{e.Venue?.Address}\",\"{e.Venue?.City?.Name}\",{e.Venue?.City?.Id},{e.Venue?.Id},{e.Id}");
        }


    }
}
