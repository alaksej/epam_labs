﻿using EpamLabs.WebPerformanceTester.Framework;
using EpamLabs.WebPerformanceTester.Models;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using OxyPlot;
using OxyPlot.Series;

namespace EpamLabs.WebPerformanceTester.ViewModels
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        private readonly System.Timers.Timer _timer;
        private Settings _settings;
        private bool _isRunning;
        private Results _results;
        private Results _previousResults;
        private readonly Stopwatch _stopwatch;
        private int _progress;
        private int _progressMax;

        private readonly LineSeries _totalRpsSeries;
        private readonly LineSeries _successfulRpsSeries;
        private readonly LineSeries _failedRpsSeries;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowViewModel()
        {
            _settings = new Settings
            {
                NumberOfThreads = 10,
                RequestsPerSecondPerThread = 5,
                Url = "https://example.com/",
                Duration = 20
            };
            RunTestCommand = new ActionCommand(_ => Run());
            CancelCommand = new ActionCommand(_ =>
            {
                TokenSource.Cancel();
            });

            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += UpdateChartData;

            _stopwatch = new Stopwatch();

            var tmp = new PlotModel();

            IsRunning = false;

            _totalRpsSeries = new LineSeries { Title = "Total", MarkerType = MarkerType.Circle};
            tmp.Series.Add(_totalRpsSeries);

            _successfulRpsSeries = new LineSeries { Title = "Successful", MarkerType = MarkerType.Star };
            tmp.Series.Add(_successfulRpsSeries);

            _failedRpsSeries = new LineSeries { Title = "Failed", MarkerType = MarkerType.Star };
            tmp.Series.Add(_failedRpsSeries);

            PlotModel = tmp;
        }

        internal CancellationTokenSource TokenSource { get; set; }

        public ICommand RunTestCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        public int Progress
        {
            get { return _progress; }
            set
            {
                if (_progress == value)
                    return;
                _progress = value;
                OnPropertyChanged(nameof(Progress));
            }
        }

        public int ProgressMax
        {
            get { return _progressMax; }
            set
            {
                if (_progressMax == value)
                    return;
                _progressMax = value;
                OnPropertyChanged(nameof(ProgressMax));
            }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                if (_isRunning == value)
                    return;
                _isRunning = value;
                OnPropertyChanged(nameof(IsRunning));
            }
        }

        public PlotModel PlotModel { get; }

        public Settings Settings
        {
            get { return _settings; } 
            set
            {
                if (_settings == value)
                    return;
                _settings = value;
                OnPropertyChanged(nameof(Settings));
            }
        }

        public Results Results
        {
            get { return _results; }
            set
            {
                if (_results == value)
                    return;
                _results = value;
                OnPropertyChanged(nameof(Results));
            }
        }

        private void OnPropertyChanged(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(v));
        }

        private void Run()
        {
            ProgressMax = Settings.Duration;

            _totalRpsSeries.Points.Clear();
            _successfulRpsSeries.Points.Clear();
            _failedRpsSeries.Points.Clear();
            _previousResults = new Results();
            _results = new Results { Settings = _settings, MinResponseTime = int.MaxValue };
            _stopwatch.Restart();
            _timer.Enabled = true;

            IsRunning = true;

            var tasks = new Task[_settings.NumberOfThreads];
            TokenSource = new CancellationTokenSource();
            
            for (var i = 0; i < _settings.NumberOfThreads; i++)
            {
                tasks[i] = Task.Factory.StartNew(() => 
                {
                    while (_stopwatch.Elapsed.Seconds < _settings.Duration)
                    {
                        Progress = _stopwatch.Elapsed.Seconds;
                        var token = TokenSource.Token;
                        if (token.IsCancellationRequested)
                        {
                            _timer.Stop();
                            _stopwatch.Stop();
                            IsRunning = false;
                            return;
                        }
                        var httpClient = new HttpClient();
                        var startRequest = _stopwatch.Elapsed;
                        HttpResponseMessage response;
                        try
                        {
                            response = httpClient.GetAsync(_settings.Url, token).Result;
                        }
                        catch (AggregateException)
                        {
                            response = new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
                        }
                        catch (OperationCanceledException)
                        {
                            _timer.Stop();
                            _stopwatch.Stop();
                            throw;
                        }

                        var requestDuration = _stopwatch.Elapsed - startRequest;
                        httpClient.Dispose();
                        UpdateResults(response, requestDuration.Milliseconds);

                        var timeout = _settings.DelayMilliseconds - (_stopwatch.Elapsed - startRequest).Milliseconds;
                        if (timeout > 0)
                        {
                            Thread.Sleep(timeout);
                        }
                    }
                });
            }

            Task.WhenAll(tasks)
                .ContinueWith((task) => 
                {
                    _timer.Stop();
                    _stopwatch.Stop();
                });
        }

        private void UpdateChartData(object sender, System.Timers.ElapsedEventArgs e)
        {
            Results.Bandwidth = (Results.TotalBytesReceived - _previousResults.TotalBytesReceived) * 8 / 1024;
            Results.TotalRequestsPerSecond = Results.TotalRequests - _previousResults.TotalRequests;
            Results.FailedRequestsPerSecond = Results.TotalErrors - _previousResults.TotalErrors;
            Results.SuccessfulRequestsPerSecond = Results.TotalRequestsPerSecond - Results.FailedRequestsPerSecond;

            _previousResults.TotalBytesReceived = Results.TotalBytesReceived;
            _previousResults.TotalRequests = Results.TotalRequests;
            _previousResults.TotalErrors = Results.TotalErrors;

            _totalRpsSeries.Points.Add(new DataPoint(_stopwatch.Elapsed.Seconds, Results.TotalRequestsPerSecond));
            _successfulRpsSeries.Points.Add(new DataPoint(_stopwatch.Elapsed.Seconds, Results.SuccessfulRequestsPerSecond));
            _failedRpsSeries.Points.Add(new DataPoint(_stopwatch.Elapsed.Seconds, Results.FailedRequestsPerSecond));
            PlotModel.InvalidatePlot(true);

            OnPropertyChanged(nameof(Results));
        }

        private void UpdateResults(HttpResponseMessage response, long elapsedMilliseconds)
        {
            Results.MinResponseTime = Results.MinResponseTime < elapsedMilliseconds
                ? Results.MinResponseTime : elapsedMilliseconds;
            Results.MaxResponseTime = Results.MaxResponseTime > elapsedMilliseconds
                ? Results.MaxResponseTime : elapsedMilliseconds;
            Results.SumOfResponseTimes += elapsedMilliseconds;
            var bytesReceived = response?.Content?.ReadAsByteArrayAsync().Result;
            if (bytesReceived != null)
                Results.TotalBytesReceived += bytesReceived.Length;
            Results.TotalRequests++;
            if (!response.IsSuccessStatusCode) Results.TotalErrors++;

            OnPropertyChanged(nameof(Results));
        }
    }
}
