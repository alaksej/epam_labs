﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamLabs.WebPerformanceTester.Caliburn.Models;
using OxyPlot;
using OxyPlot.Series;
using System.Threading;

namespace EpamLabs.WebPerformanceTester.Caliburn.Services
{
    public class PlotModelUpdater : IPlotModelUpdater
    {
        private System.Timers.Timer _timer;
        private DateTime _started;
        private int _elapsedSeconds;
        private int _intervalInSeconds;
        private int _duration;
        private CancellationTokenSource _cts;
        private CancellationToken _token;
        private Results _results;
        private Results _previousResults;
        private LineSeries _totalRpsSeries;
        private LineSeries _successfulRpsSeries;
        private LineSeries _failedRpsSeries;

        public event EventHandler PlotModelUpdated;

        public PlotModelUpdater()
        {
        }

        public PlotModel StartPlotModelUpdate(Results results, int intervalInSeconds, int duration, CancellationToken token)
        {
            _results = results;
            _intervalInSeconds = intervalInSeconds;
            _duration = duration;
            _token = token;
            _previousResults = new Results();

            var plotModel = CreatePlotModel();

            _timer = new System.Timers.Timer(intervalInSeconds * 1000);
            _timer.Elapsed += UpdateChartData;
            
            _timer.Start();
            _started = DateTime.Now;

            return plotModel;
        }

        private PlotModel CreatePlotModel()
        {
            var plotModel = new PlotModel();

            _totalRpsSeries = new LineSeries { Title = "Total", MarkerType = MarkerType.Circle };
            plotModel.Series.Add(_totalRpsSeries);

            _successfulRpsSeries = new LineSeries { Title = "Successful", MarkerType = MarkerType.Star };
            plotModel.Series.Add(_successfulRpsSeries);

            _failedRpsSeries = new LineSeries { Title = "Failed", MarkerType = MarkerType.Star };
            plotModel.Series.Add(_failedRpsSeries);

            return plotModel;
        }

        private void UpdateChartData(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(_token.IsCancellationRequested)
            {
                _timer.Stop();
                return;
            }

            _elapsedSeconds = (e.SignalTime - _started).Seconds;
            if (_elapsedSeconds > _duration)
            {
                _timer.Stop();
                return;
            }
            
            _results.Bandwidth = (_results.TotalBytesReceived - _previousResults.TotalBytesReceived) * 8 / 1024;
            _results.TotalRequestsPerSecond = _results.TotalRequests - _previousResults.TotalRequests;
            _results.FailedRequestsPerSecond = _results.TotalErrors - _previousResults.TotalErrors;
            _results.SuccessfulRequestsPerSecond = _results.TotalRequestsPerSecond - _results.FailedRequestsPerSecond;

            _previousResults.TotalBytesReceived = _results.TotalBytesReceived;
            _previousResults.TotalRequests = _results.TotalRequests;
            _previousResults.TotalErrors = _results.TotalErrors;

            _totalRpsSeries.Points.Add(new DataPoint(_elapsedSeconds, _results.TotalRequestsPerSecond));
            _successfulRpsSeries.Points.Add(new DataPoint(_elapsedSeconds, _results.SuccessfulRequestsPerSecond));
            _failedRpsSeries.Points.Add(new DataPoint(_elapsedSeconds, _results.FailedRequestsPerSecond));

            OnPlotModelUpdated();
        }

        private void OnPlotModelUpdated()
        {
            PlotModelUpdated?.Invoke(this, EventArgs.Empty);
        }
    }
}
