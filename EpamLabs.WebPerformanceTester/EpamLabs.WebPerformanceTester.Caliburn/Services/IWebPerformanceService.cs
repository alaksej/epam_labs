﻿using EpamLabs.WebPerformanceTester.Caliburn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.Services
{
    public interface IWebPerformanceService
    {
        event EventHandler ResultsUpdated;

        Results RunTest(Settings settings, CancellationTokenSource cts);
    }
}
