﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EpamLabs.WebPerformanceTester.Caliburn.Models;
using System.Net.Http;
using System.Diagnostics;

namespace EpamLabs.WebPerformanceTester.Caliburn.Services
{
    public class WebPerformanceService : IWebPerformanceService
    {
        private Results _results;

        public event EventHandler ResultsUpdated;

        public Results RunTest(Settings settings, CancellationTokenSource cts)
        {
            _results = new Results { Settings = settings, MinResponseTime = int.MaxValue };
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var tasks = new Task[settings.NumberOfThreads];

            for (var i = 0; i < settings.NumberOfThreads; i++)
            {
                tasks[i] = Task.Factory.StartNew(() =>
                {
                    while (stopwatch.Elapsed.Seconds < settings.Duration)
                    {
                        try
                        {
                            var token = cts.Token;
                            _results.Progress = stopwatch.Elapsed.Seconds;
                            if (token.IsCancellationRequested)
                            {
                                throw new OperationCanceledException();
                            }

                            var httpClient = new HttpClient();
                            var startOfRequest = stopwatch.Elapsed;

                            HttpResponseMessage response;
                            try
                            {
                                response = httpClient.GetAsync(settings.Url, token).Result;
                            }
                            catch (AggregateException)
                            {
                                response = new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
                            }

                            var requestDuration = stopwatch.Elapsed - startOfRequest;
                            httpClient.Dispose();
                            UpdateResults(response, requestDuration.Milliseconds);

                            var timeout = settings.DelayMilliseconds - (stopwatch.Elapsed - startOfRequest).Milliseconds;
                            if (timeout > 0)
                            {
                                Task.Delay(timeout, token).Wait(token);
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            stopwatch.Stop();
                            _results.IsCancelled = true;
                            OnResultsUpdated();
                        }
                    }
                });
            }

            Task.WhenAll(tasks)
                .ContinueWith((task) =>
                {
                    stopwatch.Stop();
                    _results.IsCompleted = true;
                    OnResultsUpdated();
                });

            return _results;
        }

        private void OnResultsUpdated()
        {
            ResultsUpdated?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateResults(HttpResponseMessage response, long elapsedMilliseconds)
        {
            _results.MinResponseTime = _results.MinResponseTime < elapsedMilliseconds
                ? _results.MinResponseTime : elapsedMilliseconds;
            _results.MaxResponseTime = _results.MaxResponseTime > elapsedMilliseconds
                ? _results.MaxResponseTime : elapsedMilliseconds;
            _results.SumOfResponseTimes += elapsedMilliseconds;
            var bytesReceived = response?.Content?.ReadAsByteArrayAsync().Result;
            if (bytesReceived != null)
                _results.TotalBytesReceived += bytesReceived.Length;
            _results.TotalRequests++;
            if (!response.IsSuccessStatusCode) _results.TotalErrors++;

            OnResultsUpdated();
        }
    }
}
