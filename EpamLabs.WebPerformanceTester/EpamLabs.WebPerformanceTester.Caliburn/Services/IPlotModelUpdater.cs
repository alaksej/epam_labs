﻿using EpamLabs.WebPerformanceTester.Caliburn.Models;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.Services
{
    public interface IPlotModelUpdater
    {
        event EventHandler PlotModelUpdated;

        PlotModel StartPlotModelUpdate(Results results, int intervalInSeconds, int duration, CancellationToken token);
    }
}
