﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.Models
{
    public class Settings
    {
        public int NumberOfThreads { get; set; }

        public int RequestsPerSecondPerThread { get; set; }

        public string Url { get; set; }

        public int Duration { get; set; }

        public int RequestsPerSecond => RequestsPerSecondPerThread * NumberOfThreads;

        public int TotalRequests => RequestsPerSecond * Duration;

        public int DelayMilliseconds => 1000 / RequestsPerSecond;
    }
}
