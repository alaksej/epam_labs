﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.Models
{
    public class Results
    {
        public Settings Settings { get; set; }

        public double TotalRequestsPerSecond { get; set; }

        public double SuccessfulRequestsPerSecond { get; set; }

        public double FailedRequestsPerSecond { get; set; }

        public int TotalRequests { get; set; }

        public int TotalErrors { get; set; }

        public int TotalBytesReceived { get; set; }

        public double Bandwidth { get; set; }

        public long SumOfResponseTimes { get; set; }

        public long MinResponseTime { get; set; }

        public long MaxResponseTime { get; set; }

        public double AverageResponseTime => TotalRequests == 0 ? 0 : SumOfResponseTimes / TotalRequests;

        public int Progress { get; set; }

        public bool IsCancelled { get; set; }

        public bool IsCompleted { get; set; }
    }
}