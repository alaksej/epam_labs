﻿using System.Windows;

namespace EpamLabs.WebPerformanceTester.Caliburn
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
