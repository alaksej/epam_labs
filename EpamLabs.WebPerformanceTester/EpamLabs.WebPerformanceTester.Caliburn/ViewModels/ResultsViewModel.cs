﻿using Caliburn.Micro;
using EpamLabs.WebPerformanceTester.Caliburn.Models;
using EpamLabs.WebPerformanceTester.Caliburn.Services;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.ViewModels
{
    public class ResultsViewModel : Screen
    {
        private const int PlotUpdateInterval = 1;

        public ResultsViewModel(Settings settings, IWebPerformanceService webService, IPlotModelUpdater plotUpdater)
        {
            ProgressBarIsVisible = true;

            ProgressMax = settings.Duration;

            TokenSource = new CancellationTokenSource();

            webService.ResultsUpdated += OnResultsUpdated;
            Results = webService.RunTest(settings, TokenSource);

            plotUpdater.PlotModelUpdated += OnPlotUpdated;
            PlotModel = plotUpdater.StartPlotModelUpdate(Results, PlotUpdateInterval, Results.Settings.Duration, TokenSource.Token);
        }

        public Results Results { get; set; }

        public PlotModel PlotModel { get; }

        public int ProgressMax { get; set; }

        public bool ProgressBarIsVisible { get; set; }

        public void Cancel()
        {
            TokenSource.Cancel();
            ProgressBarIsVisible = false;
            NotifyOfPropertyChange(() => ProgressBarIsVisible);
        }

        internal CancellationTokenSource TokenSource { get; set; }

        private void OnResultsUpdated(object sender, EventArgs args)
        {
            ProgressBarIsVisible = !Results.IsCompleted;
            NotifyOfPropertyChange(() => ProgressBarIsVisible);
            NotifyOfPropertyChange(() => Results);
        }

        public void OnPlotUpdated(object sender, EventArgs args)
        {
            NotifyOfPropertyChange(() => PlotModel);
            PlotModel.InvalidatePlot(true);
        }
    }
}
