﻿using Caliburn.Micro;
using EpamLabs.WebPerformanceTester.Caliburn.Models;
using EpamLabs.WebPerformanceTester.Caliburn.Services;
using EpamLabs.WebPerformanceTester.Caliburn.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamLabs.WebPerformanceTester.Caliburn.ViewModels
{
    public class MainViewModel : Screen
    {
        private readonly IWindowManager _windowManager;
        private readonly IWebPerformanceService _webService;
        private readonly IPlotModelUpdater _plotUpdater;

        public MainViewModel(
            IWindowManager windowManager, 
            IWebPerformanceService webService,
            IPlotModelUpdater plotUpdater)
        {
            _windowManager = windowManager;
            _webService = webService;
            _plotUpdater = plotUpdater;

            Settings = new Settings
            {
                NumberOfThreads = 4,
                RequestsPerSecondPerThread = 5,
                Url = "http://example.com",
                Duration = 20
            };
        }

        public Settings Settings { get; set; }

        public void RunTest()
        {
            var resultsView = new ResultsViewModel(Settings, _webService, _plotUpdater);
            _windowManager.ShowDialog(resultsView);
        }
    }
}
