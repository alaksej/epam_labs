﻿using System.Windows;

namespace EpamLabs.WebPerformanceTester.Caliburn.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
